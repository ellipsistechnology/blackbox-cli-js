# blackbox-cli

[![Version](https://img.shields.io/bitbucket/pipelines/ellipsistechnology/blackbox-cli-js.svg)](https://bitbucket.org/ellipsistechnology/blackbox-cli-js/addon/pipelines/home#!/)
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg)](https://github.com/RichardLitt/standard-readme)

> Blackbox command line interface.

The Blackbox command line interface (CLI) provides support for managing a Blackbox project. Project configuration is stored in the `blackbox.json` file and the OpenAPI definitions are stored in the `openapi.json` file. The CLI can be used to manage datatypes and services required by your project.

A Blackbox project is a project that provides a REST API conforming to the Blacbkox specification. The Blackbox specification provides standards for developing a REST API based on service and datatype definitions. The services and datatypes are defined within an OpenAPI document.

For further information about the Blackbox Specification refer to the Blackbox website.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
npm i -g blackbox-cli
```

## Usage

```
bb command [type] [options]
```

### Commands

 |Command|Description|
 |---|---|
 |`add`|Adds a Blackbox API component of a given type to the project.|
 |`delete`|Deletes an existing Blackbox API component. The component is identified by its type and name.|
 |`init`|Initialises the Blackbox project with a root OpenAPI JSON file named openapi.json and a bb.json config file.|
 |`list`|Lists all Blackbox API components of a given type that exist within the project.|
 |`update`|Changes an existing Blackbox API component. The component is identified by its type and name.|
 |`generate`|Generate code for the specified type.|

### Types

 |Type|Description|
 |---|---|
 |`datatype`|A datatype for use by services.|
 |`repository`|A datatype repository for storing and loading datatype definitions.|
 |`server`|A Blackbox server.|
 |`service`|A blackbox service.|

### Options

 |Option|Description|
 |---|---|
 |`--all, -a`|Apply operation to all relevant items. E.g. bb delete service -a -n s1 will remove service s1 and any child services.|
 |`--datatype, -d`|The data type of a service.|
 |`--desc`|The description of the API.|
 |`--dest`|The destination path for generated code.|
 |`--file, -f`|A source file to read in data appropriate to the command and type.|
 |`--methods, -m`|The HTTP methods to be supported by the service.|
 |`--name, -n`|The name of the entity being added or modified.|
 |`--service, -s`|The parent service in the case of sub-services. Defaults to no parent; i.e. a root level service.|
 |`--summary`|Summary for a service.|
 |`--title, -t`|The title of the API.|
 |`--url, -u`|A source url to read in data appropriate to the command and type.|

### Examples

Initialise the blackbox project:
```
bb init -n weather -t "A weather API." --desc \
"An API for retrieving temperature and humidity information."
```

Add a remote repository (the repository will be added to your blackbox.json):
```
bb add repository --url http://example.com/repositories/maths.json
```

Add a datatype from a local file (the datatype will be added to your openapi.json):
```
bb add datatype --name temperature --file types.json
```

Add a service:
```
bb add service --name weather --summary \
"Weather service: Provides temperature and humidity information."
```

Add a sub-service:
```
bb add service --name temperature --service weather --summary \
"Returns a temperature object."
```

Assign the datatype for a service (the datatype must be primitive, already added to the openapi.json, or exist in a repository  added to your blackbox.json; note that `--service` refers to the parent service and `--name` refers to the child service):
```
bb update service --service weather --name temperature --datatype temperature --method "post,delete"
```

Generate a server:
```
bb generate server
```

Generate datatypes:
```
bb generate datatype
```

Generate services:
```
bb generate services
```

## Maintainers

[@ellipsistechnology](https://github.com/ellipsistechnology)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Ben Millar
