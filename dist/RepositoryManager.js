"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiManager_1 = __importDefault(require("./ApiManager"));
var logging_1 = require("./logging");
var ApiManager_2 = require("./ApiManager");
var print_helper_1 = require("./print-helper");
var fs_1 = __importDefault(require("fs"));
var axios_1 = __importDefault(require("axios"));
var filenamify_url_1 = __importDefault(require("filenamify-url"));
var file_url_1 = __importDefault(require("file-url"));
var RepositoryManager = /** @class */ (function () {
    function RepositoryManager(options) {
        this.options = options;
        this.apiManager = new ApiManager_1.default();
    }
    RepositoryManager.prototype.ensureRepositories = function (bbDoc, repo) {
        if (!bbDoc.repositories)
            bbDoc.repositories = [];
        bbDoc.repositories.forEach(function (rep) {
            if (repo.file && rep.file === repo.file || repo.url && rep.url === repo.url)
                throw new Error("Repository " + JSON.stringify(repo) + " already exists in " + ApiManager_2.CONFIG_FILENAME + ".");
        });
    };
    RepositoryManager.prototype.add = function () {
        var _this = this;
        if (this.options.file !== "") {
            this.apiManager.loadConfigJson()
                .then(function (bbDoc) {
                var repo = {
                    file: _this.options.file
                };
                _this.ensureRepositories(bbDoc, repo);
                bbDoc.repositories.push(repo);
                _this.apiManager.writeConfigJson(bbDoc);
                logging_1.logem("Successfully added repository " + logging_1.code(repo.file) + ".");
            })
                .catch(function (err) { return logging_1.logerror(err); });
        }
        else if (this.options.url) {
            this.apiManager.loadConfigJson()
                .then(function (bbDoc) {
                var repo = {
                    url: _this.options.url
                };
                _this.ensureRepositories(bbDoc, repo);
                bbDoc.repositories.push(repo);
                _this.apiManager.writeConfigJson(bbDoc);
                logging_1.logem("Successfully added repository " + logging_1.code(repo.url) + ".");
            })
                .catch(function (err) { return logging_1.logerror(err); });
        }
        else {
            logging_1.logerror("Repository must be specified by either a --file or --url option.");
        }
    };
    RepositoryManager.prototype.list = function () {
        this.apiManager.loadConfigJson()
            .then(function (bbDoc) {
            print_helper_1.printList(bbDoc.repositories.map(function (repo) { return repo.file ? "file=" + repo.file : "url=" + repo.url; }));
        })
            .catch(function (err) { return logging_1.logerror(err); });
    };
    RepositoryManager.prototype.delete = function () {
        var _this = this;
        if (this.options.file === "" && this.options.url === "")
            logging_1.logerror("File or url must be provided for delete.");
        this.apiManager.loadConfigJson()
            .then(function (bbDoc) {
            var repo;
            for (var i in bbDoc.repositories) {
                if (bbDoc.repositories[i].file === _this.options.file || bbDoc.repositories[i].url === _this.options.url) {
                    repo = bbDoc.repositories[i];
                    bbDoc.repositories = bbDoc.repositories.slice(0, i).concat(bbDoc.repositories.slice(i + 1));
                    break;
                }
            }
            _this.apiManager.writeConfigJson(bbDoc);
            logging_1.logem("Successfully deleted repository " + logging_1.code(JSON.stringify(repo)) + ".");
        })
            .catch(function (err) { return logging_1.logerror(err); });
    };
    /**
     * Loads a repository from the given path.
     */
    RepositoryManager.prototype.loadRepositoryFromFile = function (path) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fs_1.default.promises.readFile(path)];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, JSON.parse(data.toString())];
                }
            });
        });
    };
    // TODO allow for authentication
    RepositoryManager.prototype.loadRepositoryFromUrl = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var path, repo, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!fs_1.default.existsSync('./.bb')) return [3 /*break*/, 2];
                        return [4 /*yield*/, fs_1.default.promises.mkdir('./.bb')];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!!fs_1.default.existsSync('./.bb/repositories')) return [3 /*break*/, 4];
                        return [4 /*yield*/, fs_1.default.promises.mkdir('./.bb/repositories')];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        path = './.bb/repositories/' + filenamify_url_1.default(url);
                        if (!path.endsWith('.json'))
                            path += '.json';
                        if (!fs_1.default.existsSync(path)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.loadRepositoryFromFile(path)];
                    case 5:
                        repo = _a.sent();
                        _a.label = 6;
                    case 6:
                        if (!!repo) return [3 /*break*/, 9];
                        logging_1.logp("Downloading repository from " + logging_1.code(url) + "...");
                        return [4 /*yield*/, axios_1.default.get(url)];
                    case 7:
                        response = _a.sent();
                        if (response.status !== 200)
                            throw new Error("Download failed: status " + response.status + " " + response.statusText + ".");
                        if (!response.headers['content-type'].includes('application/json')
                            && !response.headers['content-type'].includes('text/plain'))
                            throw new Error("Invalid response content type '" + response.headers['content-type'] + "' received from " + url + ".");
                        repo = response.data;
                        logging_1.logp("Download complete.");
                        return [4 /*yield*/, fs_1.default.promises.writeFile(path, JSON.stringify(repo))];
                    case 8:
                        _a.sent();
                        _a.label = 9;
                    case 9: return [2 /*return*/, repo];
                }
            });
        });
    };
    /**
     * Searches repositories for the given datatype.
     * @return A DataTypeWrapper containing the repository's URI and the datatype.
     */
    RepositoryManager.prototype.findRepositoryForDatatype = function (datatype) {
        return __awaiter(this, void 0, void 0, function () {
            var bbDoc, _i, _a, repo, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.apiManager.loadConfigJson()];
                    case 1:
                        bbDoc = _b.sent();
                        if (!bbDoc.repositories) {
                            return [2 /*return*/, undefined];
                        }
                        _i = 0, _a = bbDoc.repositories;
                        _b.label = 2;
                    case 2:
                        if (!(_i < _a.length)) return [3 /*break*/, 8];
                        repo = _a[_i];
                        data = void 0;
                        if (!repo.file) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.loadRepositoryFromFile(repo.file)];
                    case 3:
                        data = _b.sent();
                        return [3 /*break*/, 6];
                    case 4:
                        if (!repo.url) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.loadRepositoryFromUrl(repo.url)];
                    case 5:
                        data = _b.sent();
                        _b.label = 6;
                    case 6:
                        if (!data) {
                            logging_1.logerror("Invalid repository found in " + ApiManager_2.CONFIG_FILENAME + ": " + JSON.stringify(repo));
                        }
                        else if (data[datatype]) {
                            return [2 /*return*/, {
                                    uri: repo.url ? repo.url : file_url_1.default(repo.file),
                                    repo: data
                                }];
                        }
                        _b.label = 7;
                    case 7:
                        _i++;
                        return [3 /*break*/, 2];
                    case 8: return [2 /*return*/, undefined];
                }
            });
        });
    };
    return RepositoryManager;
}());
exports.default = RepositoryManager;
