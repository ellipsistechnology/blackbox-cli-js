declare const defaultFormat: (prefix: string, key: string, value: string) => string;
declare const fixWidth: (str: string, length: number) => string;
declare const maxLength: (strs: string[]) => number;
declare const printEnum: (Enum: any) => (prefix: string, format?: (prefix: string, key: string, value: string) => string) => string;
declare const printList: (list: string[]) => void;
export { printEnum, fixWidth, defaultFormat, maxLength, printList };
