"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var logging_1 = require("./logging");
exports.OPENAPI_FILENAME = "openapi.json";
exports.CONFIG_FILENAME = "blackbox.json";
exports.PACKAGE_FILENAME = "package.json";
exports.jsonTypes = [
    "string",
    "integer",
    "number",
    "boolean",
    "array",
    "object"
];
var ApiManager = /** @class */ (function () {
    function ApiManager() {
    }
    ApiManager.prototype.loadConfigJson = function () {
        return new Promise(function (resolve, reject) {
            fs_1.default.readFile(exports.CONFIG_FILENAME, function (err, data) {
                if (err) {
                    logging_1.logerror("Error reading Blackbox config in file " + exports.CONFIG_FILENAME
                        + ". Have you initialised your Blackbox api: bb init");
                    reject(err);
                }
                else {
                    resolve(JSON.parse(data));
                }
            });
        });
    };
    ApiManager.prototype.loadOpenApiJson = function () {
        return new Promise(function (resolve, reject) {
            fs_1.default.readFile(exports.OPENAPI_FILENAME, function (err, data) {
                if (err) {
                    logging_1.logerror("Error reading OpenAPI document in file " + exports.OPENAPI_FILENAME
                        + ". Have you initialised your Blackbox api: bb init");
                    reject(err);
                }
                else {
                    resolve(JSON.parse(data));
                }
            });
        });
    };
    ApiManager.prototype.loadPackageJson = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        fs_1.default.readFile(exports.PACKAGE_FILENAME, function (err, data) {
                            if (err) {
                                logging_1.logerror("Error reading " + exports.PACKAGE_FILENAME);
                                reject(err);
                            }
                            else {
                                resolve(JSON.parse(data));
                            }
                        });
                    })];
            });
        });
    };
    // public String loadOpenApiJsonAsString() {
    // 	try {
    // 		return Files.lines(new File(OPENAPI_FILENAME).toPath()).collect(Collectors.joining("\n"));
    // 	} catch (IOException e) {
    // 		logger.severe("Error reading OpenAPI document in file "+OPENAPI_FILENAME+". Have you initialised your Blackbox api: bb init");
    // 		if(config.verbose)
    // 			throw new RuntimeException(e);
    // 		else
    // 			return null;
    // 	}
    // }
    ApiManager.prototype.writeApiJson = function (json) {
        fs_1.default.writeFile(exports.OPENAPI_FILENAME, JSON.stringify(json, null, 2), function (err) {
            if (err)
                console.error("Failed to write to " + exports.OPENAPI_FILENAME + ": " + err);
        });
    };
    ApiManager.prototype.writeConfigJson = function (json) {
        fs_1.default.writeFile(exports.CONFIG_FILENAME, JSON.stringify(json, null, 2), function (err) {
            if (err)
                console.error("Failed to write to " + exports.CONFIG_FILENAME + ": " + err);
        });
    };
    ApiManager.prototype.writePackageJson = function (json) {
        fs_1.default.writeFile(exports.PACKAGE_FILENAME, JSON.stringify(json, null, 2), function (err) {
            if (err)
                console.error("Failed to write to " + exports.PACKAGE_FILENAME + ": " + err);
        });
    };
    // public JsonElement newJsonObject(String json) {
    // 	JsonParser parser = new JsonParser();
    // 	return parser.parse(json);
    // }
    //
    // public JsonElement newJsonObject(File file) {
    // 	JsonParser parser = new JsonParser();
    // 	try {
    // 		return parser.parse(new FileReader(file));
    // 	} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
    // 		logger.severe("Error reading json file "+file.getPath());
    // 		if(config.verbose)
    // 			throw new RuntimeException(e);
    // 		else
    // 			return null;
    // 	}
    // }
    //
    // public JsonElement getByPath(JsonObject json, String[] path) {
    // 	for (String node : path) {
    // 		JsonElement element = json.get(node);
    // 		if(element != null) {
    // 			json = element.getAsJsonObject();
    // 		}
    // 		else {
    // 			logger.severe("Path not found: "+node);
    // 			for (String p : path) {
    // 				logger.severe(p);
    // 			}
    // 			throw new NullPointerException(node);
    // 		}
    // 	}
    // 	return json;
    // }
    //
    // public JsonElement getOrCreateJsonElement(JsonElement parent, String memberName, Supplier<JsonElement> creator) {
    // 	JsonElement member = parent.getAsJsonObject().get(memberName);
    // 	if(member == null) {
    // 		member = creator.get();
    // 		parent.getAsJsonObject().add(memberName, member);
    // 	}
    // 	return member;
    // }
    //
    ApiManager.prototype.getSchemas = function (json) {
        return json.components.schemas;
    };
    ApiManager.prototype.getSchemaByPath = function (openApiRoot, path) {
        return this.getSchemaParentByPath(openApiRoot, path).schema;
    };
    ApiManager.prototype.getSchemaParentByPath = function (openApiRoot, path) {
        return openApiRoot.paths[path].get.responses['200'].content['application/json'];
    };
    return ApiManager;
}());
exports.default = ApiManager;
