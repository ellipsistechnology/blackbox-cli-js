"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var print_helper_1 = require("./print-helper");
var Types;
(function (Types) {
    Types["service"] = "A Blackbox API service.";
    Types["datatype"] = "A datatype used by a service.";
    Types["database"] = "A database access class for a single datatype.";
    Types["repository"] = "A datatype repository.";
    Types["server"] = "A Blackbox server.";
    Types["privilege"] = "User privilege/permission.";
})(Types || (Types = {}));
exports.default = Types;
exports.printTypes = print_helper_1.printEnum(Types);
