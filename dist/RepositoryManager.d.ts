import CliOptions from "./CliOptions";
import ApiManager from './ApiManager';
export interface RepoWrapper {
    uri: string;
    repo: any;
}
export default class RepositoryManager {
    options: CliOptions;
    apiManager: ApiManager;
    constructor(options: CliOptions);
    private ensureRepositories;
    add(): void;
    list(): void;
    delete(): void;
    /**
     * Loads a repository from the given path.
     */
    loadRepositoryFromFile(path: string): Promise<any>;
    loadRepositoryFromUrl(url: string): Promise<any>;
    /**
     * Searches repositories for the given datatype.
     * @return A DataTypeWrapper containing the repository's URI and the datatype.
     */
    findRepositoryForDatatype(datatype: string): Promise<RepoWrapper | undefined>;
}
