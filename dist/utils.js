"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiManager_1 = require("./ApiManager");
function toCammelCase(name, startUpper) {
    if (startUpper === void 0) { startUpper = true; }
    if (ApiManager_1.jsonTypes.includes(name) || name.endsWith('[]') && ApiManager_1.jsonTypes.includes(name.substring(0, name.length - 2))) {
        return name;
    }
    else {
        var cc = name.replace(/([-_][a-z])/ig, function ($1) {
            return $1.toUpperCase()
                .replace('-', '')
                .replace('_', '');
        });
        if (startUpper) {
            return cc.charAt(0).toUpperCase() + cc.substring(1);
        }
        else {
            return cc;
        }
    }
    // name.split('-')
    //   .map((part, i) => i > 0 || startUpper ? part.charAt(0).toUpperCase() + part.substring(1) : part)
    //   .join('')
}
exports.toCammelCase = toCammelCase;
