interface TemplateMap {
    name: string;
    parentClasses?: string[];
    properties: {
        type: string;
        name: string;
    }[];
    values?: string[];
    required: string[];
}
export default function applyDatetypeClassTemplate({ name, properties, parentClasses, values, required }: TemplateMap): string;
export {};
