export declare const OPENAPI_FILENAME: string;
export declare const CONFIG_FILENAME: string;
export declare const PACKAGE_FILENAME: string;
export declare const jsonTypes: string[];
export default class ApiManager {
    constructor();
    loadConfigJson(): Promise<any>;
    loadOpenApiJson(): Promise<any>;
    loadPackageJson(): Promise<any>;
    writeApiJson(json: any): void;
    writeConfigJson(json: any): void;
    writePackageJson(json: any): void;
    getSchemas(json: any): any;
    getSchemaByPath(openApiRoot: any, path: string): any;
    getSchemaParentByPath(openApiRoot: any, path: string): any;
}
