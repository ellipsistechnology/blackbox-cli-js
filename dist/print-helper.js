"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logging_1 = require("./logging");
var defaultFormat = function (prefix, key, value) { return "" + prefix + key + " " + value; };
exports.defaultFormat = defaultFormat;
var fixWidth = function (str, length) {
    var padding = length - str.length + 2;
    return str + Array(padding).fill(' ').join('');
};
exports.fixWidth = fixWidth;
var maxLength = function (strs) { return (strs
    .map(function (key) { return key.length; })
    .reduce(function (accumulator, currentValue) { return Math.max(accumulator, currentValue); }, 0)); };
exports.maxLength = maxLength;
var printEnum = function (Enum) {
    var colWidth = maxLength(Object.keys(Enum));
    return function (prefix, format) {
        if (format === void 0) { format = defaultFormat; }
        return (Object.keys(Enum)
            .map(function (key) { return format(prefix, fixWidth(key, colWidth), Enum[key]); })
            .reduce(function (accumulator, currentValue) { return accumulator + currentValue + '\n'; }, ''));
    };
};
exports.printEnum = printEnum;
var printList = function (list) {
    var col = 0;
    var line = '';
    for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
        var item = list_1[_i];
        ++col;
        line += item + "\t\t";
        if (col === 5) {
            logging_1.logcode(line);
            line = '';
            col = 0;
        }
    }
    if (line !== '')
        logging_1.logcode(line);
};
exports.printList = printList;
