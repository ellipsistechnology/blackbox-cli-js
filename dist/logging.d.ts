export declare function setVerboseLogging(v: boolean): void;
declare const h1: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const code: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const error: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const warn: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const em: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const p: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const desc: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const file: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const q: import("chalk").Chalk & {
    supportsColor: import("chalk").ColorSupport;
};
declare const logh1: (str: string) => void;
declare const logcode: (str: string) => void;
declare const logem: (str: string) => void;
declare const logp: (str: string) => void;
declare const logerror: (message: string, err?: any) => void;
declare const logwarn: (str: string) => void;
declare const logdesc: (str: string) => void;
declare const logfile: (str: string) => void;
declare const logq: (str: string) => void;
export { h1, code, em, p, error, desc, file, q, warn, logh1, logcode, logem, logp, logerror, logdesc, logfile, logq, logwarn };
