export default class CliOptions {
    name: string;
    title: string;
    description: string;
    service: string;
    datatype: string;
    methods: string;
    all: boolean;
    summary: String;
    file: string;
    url: string;
    dest: string;
    verbose: boolean;
    singular: string;
    sourceData: string | undefined;
    constructor(args: string[]);
    servicePath(): string;
    serviceTag(): string;
    describeOptions(prefix: string, format?: (prefix: string, key: string, value: string) => string): string;
    methodsSet(): string[];
    getFileData(): any;
}
