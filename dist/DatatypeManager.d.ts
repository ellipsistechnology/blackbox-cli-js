import CliOptions from './CliOptions';
import ApiManager from './ApiManager';
import { RepoWrapper } from './RepositoryManager';
export default class DatatypeManager {
    options: CliOptions;
    apiManager: ApiManager;
    constructor(options: CliOptions);
    private deepCopy;
    private allRefs;
    addDependantDatatypes(openApiJson: any, repoWrapper: RepoWrapper, name: string): void;
    private mapRefs;
    private putDatatype;
    add(): Promise<void>;
    private updateByPrompt;
    private addField;
    printTypes(schemas: any): void;
    update(): Promise<void>;
    list(): void;
    datatypeInUse(openApiJson: any): boolean;
    delete(): void;
}
