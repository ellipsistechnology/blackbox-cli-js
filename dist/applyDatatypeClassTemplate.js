"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiManager_1 = require("./ApiManager");
var Generator_1 = require("./Generator");
var utils_1 = require("./utils");
function applyDatetypeClassTemplate(_a) {
    var name = _a.name, properties = _a.properties, parentClasses = _a.parentClasses, values = _a.values, required = _a.required;
    var className = utils_1.toCammelCase(name);
    var imports = '';
    new Set(properties // use a set to remove duplicates
        .map(function (property) { return property.type; }) // extract types from properties
        .map(function (type) { return type.endsWith('[]') ? type.substring(0, type.length - 2) : type; }) // extract type from array
        .reduce(function (classes, type) { return classes.concat(type.split('&')); }, []) // split Type1&Type2
        .reduce(function (classes, type) { return classes.concat(type.split('|')); }, []) // split Type1|Type2
        .filter(function (type) { return type && type !== 'any' && type !== 'undefined' && !ApiManager_1.jsonTypes.includes(type); }) // remove empty && json types etc.
        .map(function (name) { return utils_1.toCammelCase(name); })).forEach(function (type) { return imports += Generator_1.importMap[type] ? "import {" + type + "} from '" + Generator_1.importMap[type] + "'\n" : "import " + type + " from './" + type + "'\n"; });
    var classOrInterfaceOrEnum = values ? 'enum' : 'interface';
    // /${pattern}/.test(this.toString())
    if (parentClasses)
        parentClasses.filter(function (className) { return !ApiManager_1.jsonTypes.includes(className.toLowerCase()); }).forEach(function (className) { return imports += "import {" + className + "} from './" + className + "'\n"; });
    return "" + (imports ? imports + '\n' : '') + (!values ? 'export default ' : '') + classOrInterfaceOrEnum + " " + className + (parentClasses ? ' extends ' + parentClasses.join(', ') : '') + " {\n  " + properties.map(function (property) { return "" + utils_1.toCammelCase(property.name, false) + (required.includes(property.name) ? '' : '?') + ": " + utils_1.toCammelCase(property.type); }).join('\n  ') + (values ? values.join(',\n  ') : '') + "\n}\n" + (values ? "export default " + className : '') + "\n";
}
exports.default = applyDatetypeClassTemplate;
