"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function applyDatabaseTemplate(className, type) {
    return "import {database} from 'blackbox-database'\nimport " + className + " from './" + className + "';\nimport {named} from 'blackbox-ioc';\n\n@database()\n@named('" + type + "-database')\nexport default class " + className + "Database {\n  async get" + className + "(_name:string): Promise<" + className + "> {return <" + className + ">{}}\n  async get" + className + "List(): Promise<" + className + "[]> {return <" + className + "[]>[]}\n  async create" + className + "(_" + type.replace('-', '') + ": " + className + ") {}\n  async update" + className + "(_name: string, _" + type.replace('-', '') + ": " + className + ") {}\n  async delete" + className + "(_name: string) {}\n}\n";
}
exports.default = applyDatabaseTemplate;
