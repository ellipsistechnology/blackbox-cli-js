import CliOptions from './CliOptions';
import ApiManager from './ApiManager';
export default class ServiceManager {
    options: CliOptions;
    apiManager: ApiManager;
    constructor(options: CliOptions);
    add(): Promise<void>;
    update(): Promise<void>;
    private addPathForService;
    private makeOperationId;
    private addServiceMethod;
    private schema;
    private httpGetResponseSchema;
    private addServiceGet;
    private addServiceWithName;
    private setupServiceWithNameGet;
    private setServiceDatatype;
    list(): void;
    delete(): void;
}
