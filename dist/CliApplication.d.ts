import Types from './Types';
import CliOptions from './CliOptions';
import ServiceManager from './ServiceManager';
import DatatypeManager from './DatatypeManager';
import RepositoryManager from './RepositoryManager';
export default class CliApplication {
    args: string[];
    options: CliOptions;
    constructor(args: string[]);
    printHelp(): void;
    run(): Promise<void>;
    executeManagerMethod(command: string): void;
    manager(type: Types): RepositoryManager | DatatypeManager | ServiceManager | null;
}
