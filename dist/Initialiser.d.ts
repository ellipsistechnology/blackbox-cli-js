import CliOptions from './CliOptions';
export default class Initialiser {
    options: CliOptions;
    constructor(opts: CliOptions);
    init(): void;
    initBlackbox(): boolean;
    initOpenAPI(): boolean;
    addInfo(info: any): void;
}
