"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = __importDefault(require("chalk"));
var verboseLogging = false;
function setVerboseLogging(v) {
    verboseLogging = v;
}
exports.setVerboseLogging = setVerboseLogging;
var logger = function (f) { return function (str) { return console.log(f(str)); }; };
var h1 = chalk_1.default.bold;
exports.h1 = h1;
var code = chalk_1.default.cyan;
exports.code = code;
var error = chalk_1.default.redBright;
exports.error = error;
var warn = chalk_1.default.yellow;
exports.warn = warn;
var em = chalk_1.default.bold.green;
exports.em = em;
var p = chalk_1.default.reset;
exports.p = p;
var desc = chalk_1.default.green;
exports.desc = desc;
var file = chalk_1.default.red;
exports.file = file;
var q = chalk_1.default.cyan;
exports.q = q;
var logh1 = logger(h1);
exports.logh1 = logh1;
var logcode = logger(code);
exports.logcode = logcode;
var logem = logger(em);
exports.logem = logem;
var logp = logger(p);
exports.logp = logp;
var logerror = function (message, err) {
    if (err === void 0) { err = undefined; }
    logger(error)(message);
    if (verboseLogging && err)
        console.log(err);
};
exports.logerror = logerror;
var logwarn = logger(warn);
exports.logwarn = logwarn;
var logdesc = logger(desc);
exports.logdesc = logdesc;
var logfile = logger(file);
exports.logfile = logfile;
var logq = logger(q);
exports.logq = logq;
