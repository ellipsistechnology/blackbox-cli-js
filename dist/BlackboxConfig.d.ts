export default class BlackboxConfig {
    name: string;
    verbose: boolean;
}
