"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiManager_1 = __importStar(require("./ApiManager"));
var logging_1 = require("./logging");
var object_1 = __importDefault(require("json-templater/object"));
var print_helper_1 = require("./print-helper");
var readline_sync_1 = __importDefault(require("readline-sync"));
var post_template_json_1 = __importDefault(require("./templates/post-template.json"));
var put_template_json_1 = __importDefault(require("./templates/put-template.json"));
var patch_template_json_1 = __importDefault(require("./templates/patch-template.json"));
var delete_template_json_1 = __importDefault(require("./templates/delete-template.json"));
var service_template_json_1 = __importDefault(require("./templates/service-template.json"));
var RepositoryManager_1 = __importDefault(require("./RepositoryManager"));
var DatatypeManager_1 = __importDefault(require("./DatatypeManager"));
var utils_1 = require("./utils");
var methodToOperation = {
    get: "get",
    post: "create",
    put: "replace",
    patch: "update",
    delete: "delete",
    options: "getOptionsFor"
};
var ServiceManager = /** @class */ (function () {
    function ServiceManager(options) {
        this.options = options;
        this.apiManager = new ApiManager_1.default();
    }
    ServiceManager.prototype.add = function () {
        return __awaiter(this, void 0, void 0, function () {
            var openApiRoot, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // Validate options:
                        if (this.options.name === '') {
                            logging_1.logerror("Name not found. Use -n or --name to supply a name for the service you wish to add.");
                            return [2 /*return*/];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        return [4 /*yield*/, this.apiManager.loadOpenApiJson()];
                    case 2:
                        openApiRoot = _a.sent();
                        this.addPathForService(openApiRoot);
                        if (!(this.options.datatype !== '')) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.setServiceDatatype(openApiRoot)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        this.apiManager.writeApiJson(openApiRoot);
                        logging_1.logem("Successfully added service " + logging_1.code(this.options.name));
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        logging_1.logerror("Failed to add service: " + err_1, err_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    // TODO: allow for updating methods
    ServiceManager.prototype.update = function () {
        return __awaiter(this, void 0, void 0, function () {
            var openApiJson, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.options.datatype !== '')) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, this.apiManager.loadOpenApiJson()];
                    case 2:
                        openApiJson = _a.sent();
                        return [4 /*yield*/, this.setServiceDatatype(openApiJson)];
                    case 3:
                        _a.sent();
                        this.apiManager.writeApiJson(openApiJson);
                        logging_1.logem("Successfully updated service " + logging_1.code(this.options.servicePath()) + " with datatype " + logging_1.code(this.options.datatype));
                        return [3 /*break*/, 5];
                    case 4:
                        err_2 = _a.sent();
                        logging_1.logerror("Failed to update service: " + err_2, err_2);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ServiceManager.prototype.addPathForService = function (openApiRoot) {
        var path = this.options.servicePath();
        if (openApiRoot.paths[path]) {
            throw new Error("A service already exists at path " + path);
        }
        else {
            var service = object_1.default(service_template_json_1.default, {
                summary: this.options.summary ? this.options.summary : '',
                tag: this.options.serviceTag(),
                getOperationId: this.makeOperationId('get', 'Service'),
                optionsOperationId: this.makeOperationId('options', 'Service'),
                parameters: [
                    { "$ref": "#/components/parameters/query-depth" },
                    { "$ref": "#/components/parameters/query-verbose" }
                ]
            });
            openApiRoot.paths[path] = service;
            logging_1.logp("Added path " + logging_1.code(path));
        }
        return true;
    };
    ServiceManager.prototype.makeOperationId = function (method, suffix) {
        if (suffix === void 0) { suffix = ''; }
        var name = this.options.singular ? this.options.singular : this.options.name;
        return (methodToOperation[method] +
            utils_1.toCammelCase(name) +
            suffix);
    };
    ServiceManager.prototype.addServiceMethod = function (servicePath, httpMethod, openApiJson, methodTemplate, templateMap) {
        templateMap = Object.assign({ operationId: this.makeOperationId(httpMethod) }, templateMap);
        var serviceElement = openApiJson.paths[servicePath];
        if (!serviceElement[httpMethod]) {
            var method = object_1.default(methodTemplate, templateMap);
            if (method) {
                if (method.requestBody)
                    method.requestBody.content['application/json'].schema = this.schema();
                serviceElement[httpMethod] = method;
                logging_1.logp("Added " + logging_1.code(httpMethod) + " to path " + logging_1.code(servicePath));
            }
            else {
                logging_1.logerror("Error loading template for method " + methodTemplate);
            }
        }
    };
    ServiceManager.prototype.schema = function () {
        if (ApiManager_1.jsonTypes.includes(this.options.datatype))
            return { type: this.options.datatype };
        else
            return { "$ref": "#/components/schemas/" + this.options.datatype };
    };
    ServiceManager.prototype.httpGetResponseSchema = function () {
        if (!ApiManager_1.jsonTypes.includes(this.options.datatype)) {
            return {
                allOf: [
                    this.schema(),
                    { "$ref": "#/components/schemas/verbose-object" }
                ]
            };
        }
        else {
            return this.schema();
        }
    };
    ServiceManager.prototype.addServiceGet = function (openApiJson) {
        var path = this.options.servicePath();
        var getSchemaParent = this.apiManager.getSchemaParentByPath(openApiJson, path);
        getSchemaParent.schema = {
            type: "array",
            items: this.httpGetResponseSchema()
        };
        if (this.options.summary)
            openApiJson.paths[path].get.summary = this.options.summary;
        else if (!openApiJson.paths[path].get.summary)
            openApiJson.paths[path].get.summary = "Retrieve a list of " + this.options.datatype + " objects.";
        openApiJson.paths[path].get.operationId = this.makeOperationId('get', 'List');
        logging_1.logp("Set get schema for path " + logging_1.code(path));
    };
    ServiceManager.prototype.addServiceWithName = function (openApiJson, templateMap) {
        var serviceName = this.options.servicePath() + "/{name}";
        var service = openApiJson.paths[serviceName];
        var getOperationId = this.makeOperationId('get');
        var optionsOperationId = this.makeOperationId('options');
        if (!service) {
            service = object_1.default(service_template_json_1.default, {
                summary: "Gets a " + this.options.name + " by name.",
                tag: this.options.serviceTag(),
                getOperationId: getOperationId,
                optionsOperationId: optionsOperationId
            });
            service.options.parameters = service.get.parameters = [
                {
                    "$ref": "#/components/parameters/path-name"
                },
                {
                    "$ref": "#/components/parameters/query-verbose"
                },
                {
                    "$ref": "#/components/parameters/query-depth"
                }
            ];
            openApiJson.paths[serviceName] = service;
            logging_1.logp("Addded service to path " + logging_1.code(serviceName));
        }
        else {
            service.operationId = getOperationId;
        }
        return service;
    };
    ServiceManager.prototype.setupServiceWithNameGet = function (service) {
        var tags = service.get.tags;
        if (!tags) {
            tags = [];
            service.get.tags = tags;
        }
        var tag = this.options.service ? this.options.service : this.options.name;
        if (!tags.includes(tag))
            tags.push(tag);
        service.get.responses[200].content["application/json"].schema = this.httpGetResponseSchema();
    };
    ServiceManager.prototype.setServiceDatatype = function (openApiJson) {
        return __awaiter(this, void 0, void 0, function () {
            var repo, service, templateData, methods, serviceWithName, serviceWithNamePath;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new RepositoryManager_1.default(this.options).findRepositoryForDatatype(this.options.datatype)];
                    case 1:
                        repo = _a.sent();
                        if (repo) {
                            new DatatypeManager_1.default(this.options).addDependantDatatypes(openApiJson, repo, this.options.datatype);
                            service = openApiJson.paths[this.options.servicePath()];
                            if (!service)
                                throw new Error("Service not found at path " + this.options.servicePath());
                            if (!service['x-blackbox-types'])
                                service['x-blackbox-types'] = [];
                            service['x-blackbox-types'].push({
                                uri: repo.uri,
                                name: this.options.datatype
                            });
                        }
                        templateData = {
                            datatype: this.options.datatype,
                            tag: this.options.serviceTag(),
                            service: this.options.name
                        };
                        methods = this.options.methodsSet();
                        this.addServiceGet(openApiJson); // FIXME: generating incorrect operationId when getting here from add()
                        if (methods.includes("post"))
                            this.addServiceMethod(this.options.servicePath(), "post", openApiJson, post_template_json_1.default, templateData);
                        if (methods.includes("put") || methods.includes("patch") || methods.includes("delete")) {
                            serviceWithName = this.addServiceWithName(openApiJson, templateData);
                            this.setupServiceWithNameGet(serviceWithName);
                            serviceWithNamePath = this.options.servicePath() + "/{name}";
                            if (methods.includes("put"))
                                this.addServiceMethod(serviceWithNamePath, "put", openApiJson, put_template_json_1.default, templateData);
                            if (methods.includes("patch"))
                                this.addServiceMethod(serviceWithNamePath, "patch", openApiJson, patch_template_json_1.default, templateData);
                            if (methods.includes("delete"))
                                this.addServiceMethod(serviceWithNamePath, "delete", openApiJson, delete_template_json_1.default, templateData);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ServiceManager.prototype.list = function () {
        this.apiManager.loadOpenApiJson()
            .then(function (openApiDoc) {
            print_helper_1.printList(Object.keys(openApiDoc.paths));
        })
            .catch(function (err) {
            logging_1.logerror("Error loading openAPI document");
        });
    };
    ServiceManager.prototype.delete = function () {
        var _this = this;
        // Validate parameters:
        if (this.options.name === '') {
            logging_1.logerror("Name not found. Use -n or --name to supply a name for the service you wish to delete.");
            return;
        }
        // Prompt user for confirmation to delete:
        var message = "Are you sure you want to delete service " + this.options.servicePath() + (this.options.all ? " and all it's children" : "") + "? (y/n): ";
        if (readline_sync_1.default.question(message) === 'y') {
            this.apiManager.loadOpenApiJson()
                .then(function (openApiJson) {
                // Find the path to delete:
                var servicePath = _this.options.servicePath();
                if (openApiJson.paths[servicePath]) {
                    // Delete the service:
                    logging_1.logp("Deleting path " + logging_1.code(servicePath));
                    openApiJson.paths[servicePath] = undefined;
                    var servicePathWithName = servicePath + "/{name}";
                    if (openApiJson.paths[servicePathWithName]) {
                        logging_1.logp("Deleting path " + logging_1.code(servicePathWithName));
                        openApiJson.paths[servicePathWithName] = undefined;
                    }
                    // Delete any child services:
                    if (_this.options.all) {
                        Object.keys(openApiJson.paths)
                            .filter(function (key) { return key.startsWith(servicePath + "/"); })
                            .forEach(function (key) {
                            logging_1.logp("Deleting path " + logging_1.code(key));
                            openApiJson.paths[key] = undefined;
                        });
                    }
                    // Write changes:
                    _this.apiManager.writeApiJson(openApiJson);
                    logging_1.logem("Successfully deleted service " + logging_1.code(servicePath) + ".");
                }
                else {
                    logging_1.logerror("Service not found for path " + logging_1.code(servicePath) + ".");
                }
            })
                .catch(function (err) {
                logging_1.logerror("Error loading openAPI document: " + err, err);
            });
        }
    };
    return ServiceManager;
}());
exports.default = ServiceManager;
