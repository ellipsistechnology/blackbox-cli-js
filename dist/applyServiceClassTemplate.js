"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Generator_1 = require("./Generator");
var utils_1 = require("./utils");
function applyServiceClassTemplate(_a) {
    var name = _a.name, methods = _a.methods;
    var className = utils_1.toCammelCase(name);
    var classVarName = utils_1.toCammelCase(name, false);
    var imports = '';
    new Set(methods // use a set to remove duplicates
        .map(function (method) { return method.type; }) // extract types from methods
        .filter(function (type) { return type; }) // remove empty
        .map(function (type) { return type.endsWith('[]') ? type.substring(0, type.length - 2) : type; })
        .reduce(function (classes, type) { return classes.concat(type.split('&')); }, []) // split Type1&Type2
        .reduce(function (classes, type) { return classes.concat(type.split('|')); }, []) // split Type1|Type2
    ).forEach(function (type) { return imports += Generator_1.importMap[type] ? "import {" + type + "} from '" + Generator_1.importMap[type] + "'\n" : "import " + type + " from './" + type + "'\n"; });
    return "import {serviceClass, autowired} from 'blackbox-ioc'\nimport {makeServiceObject} from 'blackbox-services'\n" + imports + "\n\n@serviceClass('" + classVarName + "-service')\nexport class " + className + "ServiceImpl {\n\n  @autowired('oasDoc')\n  oasDoc:any\n\n  constructor() {\n  }\n  " + methods.map(function (method) { return "\n  " + method.operationId + "(props:any)" + (method.type ? ":" + method.type : '') + " {\n    " + (method.type === 'Service' ?
        "return makeServiceObject(this.oasDoc, '" + name + "')" :
        "throw new Error(`" + method.operationId + " not yet implemented. Called with props=${JSON.stringify(props)}`)") + "\n  }"; }).join('\n') + "\n}\n";
}
exports.default = applyServiceClassTemplate;
