interface TemplateMap {
    name: string;
    methods: {
        type: string;
        operationId: string;
    }[];
}
export default function applyServiceClassTemplate({ name, methods }: TemplateMap): string;
export {};
