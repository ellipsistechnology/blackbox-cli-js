"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var appConfig_1 = __importDefault(require("./appConfig"));
var ApiManager_1 = __importDefault(require("./ApiManager"));
var openapi_template_json_1 = __importDefault(require("./templates/openapi-template.json"));
var ApiManager_2 = require("./ApiManager");
var logging_1 = require("./logging");
// const OPENAPI_TEMPLATE_FILENAME:string = "openapi-template.json";
var Initialiser = /** @class */ (function () {
    function Initialiser(opts) {
        this.options = opts;
    }
    Initialiser.prototype.init = function () {
        // Validate options:
        if (this.options.name === '') {
            logging_1.logerror("Name not found. Use -n or --name to supply a name for the API.");
            return;
        }
        if (fs_1.default.existsSync(ApiManager_2.CONFIG_FILENAME)) {
            logging_1.logerror(ApiManager_2.CONFIG_FILENAME + " file already exists. Has your project already been initialised?");
            return;
        }
        if (fs_1.default.existsSync(ApiManager_2.OPENAPI_FILENAME)) {
            logging_1.logerror(ApiManager_2.OPENAPI_FILENAME + " file already exists. Has your project already been initialised?");
            return;
        }
        // Setup config files:
        if (this.initBlackbox() && this.initOpenAPI()) {
            logging_1.logem("Blackbox project successfully initialised.");
        }
    };
    Initialiser.prototype.initBlackbox = function () {
        try {
            fs_1.default.writeFileSync(ApiManager_2.CONFIG_FILENAME, "{\n  \"name\": \"" + this.options.name + "\"\n}\n");
        }
        catch (err) {
            logging_1.logerror("An error occured while writing to " + ApiManager_2.CONFIG_FILENAME + ": " + err);
            return false;
        }
        logging_1.logp("Created " + logging_1.file(ApiManager_2.CONFIG_FILENAME));
        return true;
    };
    Initialiser.prototype.initOpenAPI = function () {
        // fs.readFile(OPENAPI_TEMPLATE_FILENAME, (err, data) => {
        //   if(err) {
        //     console.error(`Error reading file ${OPENAPI_TEMPLATE_FILENAME}: ${err}`)
        //     return
        //   }
        // let openApiTemplate = JSON.parse(data.toString());
        this.addInfo(openapi_template_json_1.default.info);
        new ApiManager_1.default().writeApiJson(openapi_template_json_1.default); // WARNING: ApiManager must be created here since the blackbox config will not have been created in advance
        // })
        logging_1.logp("Created " + logging_1.file(ApiManager_2.OPENAPI_FILENAME));
        return true;
    };
    Initialiser.prototype.addInfo = function (info) {
        info.version = appConfig_1.default.version;
        info.title = this.options.title;
        if (this.options.description && this.options.description !== '')
            info.description = this.options.description;
    };
    return Initialiser;
}());
exports.default = Initialiser;
