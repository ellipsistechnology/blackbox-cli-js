"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BlackboxConfig = /** @class */ (function () {
    function BlackboxConfig() {
        this.name = "New Blackbox API";
        this.verbose = false;
    }
    return BlackboxConfig;
}());
exports.default = BlackboxConfig;
