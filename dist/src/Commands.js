"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var print_helper_1 = require("./print-helper");
var Commands;
(function (Commands) {
    Commands["init"] = "Initialise a Blackbox API project with an OpenAPI document.";
    Commands["add"] = "Add a new item; must provide a type.";
    Commands["update"] = "Update an existing item; must provide a type.";
    Commands["list"] = "Shows a list of items; must provide a type.";
    Commands["delete"] = "Delete an itesm; must provide a type.";
    Commands["analyse"] = "Analyses the API and attempts to identify any non-compliance.";
    Commands["generate"] = "Generate server code from the OpenAPI document.";
})(Commands || (Commands = {}));
exports.default = Commands;
exports.printCommands = print_helper_1.printEnum(Commands);
