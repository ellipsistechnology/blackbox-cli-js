"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ApiManager_1 = __importStar(require("./ApiManager"));
var logging_1 = require("./logging");
var readline_sync_1 = __importDefault(require("readline-sync"));
var print_helper_1 = require("./print-helper");
var RepositoryManager_1 = __importDefault(require("./RepositoryManager"));
// TODO allow add from repository
var DatatypeManager = /** @class */ (function () {
    function DatatypeManager(options) {
        this.options = options;
        this.apiManager = new ApiManager_1.default();
    }
    DatatypeManager.prototype.deepCopy = function (src) {
        var _this = this;
        var copy;
        if (Array.isArray(src)) {
            copy = src.map(function (entry) { return _this.deepCopy(entry); });
        }
        else if (ApiManager_1.jsonTypes.includes(typeof src)) {
            copy = src;
        }
        else {
            copy = {};
            Object.keys(src).forEach(function (key) {
                copy[key] = _this.deepCopy(src[key]);
            });
        }
        return copy;
    };
    DatatypeManager.prototype.allRefs = function (src) {
        return src.filter(function (entry) { return entry["$ref"] !== undefined; });
    };
    DatatypeManager.prototype.addDependantDatatypes = function (openApiJson, repoWrapper, name) {
        var _this = this;
        var add = function (name) {
            var copy = _this.deepCopy(repoWrapper.repo[name]);
            var toAdd = [];
            var refNodes = [];
            // Find any $refs in oneOf, anyOf, allOf:
            if (Array.isArray(copy.oneOf)) {
                refNodes = _this.allRefs(copy.oneOf);
            }
            else if (Array.isArray(copy.allOf)) {
                refNodes = _this.allRefs(copy.allOf);
            }
            else if (Array.isArray(copy.anyOf)) {
                refNodes = _this.allRefs(copy.anyOf);
            }
            else if (copy.properties) {
                // Find children that are also references within the repository:
                Object.keys(copy.properties).forEach(function (key) {
                    // Property is a $ref:
                    if (copy.properties[key]["$ref"]) {
                        refNodes.push(copy.properties[key]);
                    }
                    // Property is an array with items that are $ref:
                    else if (copy.properties[key].type
                        && copy.properties[key].type === "array"
                        && copy.properties[key].items
                        && copy.properties[key].items["$ref"]) {
                        refNodes.push(copy.properties[key].items);
                    }
                    // Property includes $refs in oneOf, anyOf, allOf:
                    else if (Array.isArray(copy.properties[key].oneOf)) {
                        refNodes = refNodes.concat(_this.allRefs(copy.properties[key].oneOf));
                    }
                    else if (Array.isArray(copy.properties[key].anyOf)) {
                        refNodes = refNodes.concat(_this.allRefs(copy.properties[key].anyOf));
                    }
                    else if (Array.isArray(copy.properties[key].allOf)) {
                        refNodes = refNodes.concat(_this.allRefs(copy.properties[key].allOf));
                    }
                });
            }
            // Update $ref:
            refNodes.forEach(function (n) {
                var value = n["$ref"];
                var newRef = /(?<=^#\/)[^\/]+$/.exec(value); // Strip out #/
                if (newRef) {
                    n["$ref"] = "#/components/schemas/" + newRef[0];
                    toAdd.push(newRef[0]);
                }
                else {
                    throw new Error("Failed to parse path " + value + " in repository: Note that all $refs must start with #/ and be relative to the root of the repository.");
                }
            });
            // Recurse on children:
            toAdd.forEach(add);
            // Add the datatype to openapi.json:
            openApiJson.components.schemas[name] = copy;
            logging_1.logp("Added datatype " + logging_1.code(name) + " to " + logging_1.code(ApiManager_1.OPENAPI_FILENAME) + " from repository.");
        };
        add(name);
    };
    DatatypeManager.prototype.mapRefs = function (datatype) {
        if (datatype.properties) {
            Object.values(datatype.properties).forEach(function (prop) {
                if (typeof prop['$ref'] === 'string') {
                    prop['$ref'] = prop['$ref'].replace('#/', '#/components/schemas/');
                }
                else if (prop.type === 'array' && prop.items && typeof prop.items['$ref'] === 'string') {
                    prop.items['$ref'] = prop.items['$ref'].replace('#/', '#/components/schemas/');
                }
            });
        }
    };
    DatatypeManager.prototype.putDatatype = function (openApiDoc, name, datatype, allowReplace, repoWrapper) {
        if (datatype === void 0) { datatype = undefined; }
        if (allowReplace === void 0) { allowReplace = false; }
        if (repoWrapper === void 0) { repoWrapper = undefined; }
        var schemas = this.apiManager.getSchemas(openApiDoc);
        if (!allowReplace && schemas[this.options.name]) {
            logging_1.logerror("Datatype with name " + name + " already exists; Datatype not added.");
            return;
        }
        if (!datatype) {
            datatype = { "type": "object", "properties": {} };
            this.updateByPrompt(schemas, datatype);
        }
        if (repoWrapper) {
            this.addDependantDatatypes(openApiDoc, repoWrapper, this.options.name);
        }
        else {
            this.mapRefs(datatype);
            schemas[name] = datatype;
        }
        this.apiManager.writeApiJson(openApiDoc);
        logging_1.logem("Successfully " + (allowReplace ? 'updated' : 'added') + " datatype " + name);
    };
    DatatypeManager.prototype.add = function () {
        return __awaiter(this, void 0, void 0, function () {
            var openApiDoc_1, source_1, repoWrapper, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, , 7]);
                        return [4 /*yield*/, this.apiManager.loadOpenApiJson()
                            // If source file is provided then read from that:
                        ];
                    case 1:
                        openApiDoc_1 = _a.sent();
                        if (!(this.options.file !== "")) return [3 /*break*/, 2];
                        source_1 = this.options.getFileData();
                        // No name means add all:
                        if (this.options.name === '')
                            Object.keys(source_1).forEach(function (name) { return _this.putDatatype(openApiDoc_1, name, source_1[name]); });
                        // Otherwise just add the type with the given name:
                        else
                            this.putDatatype(openApiDoc_1, this.options.name, source_1[this.options.name]);
                        return [3 /*break*/, 5];
                    case 2:
                        if (!(this.options.name !== '')) return [3 /*break*/, 4];
                        return [4 /*yield*/, new RepositoryManager_1.default(this.options).findRepositoryForDatatype(this.options.name)];
                    case 3:
                        repoWrapper = _a.sent();
                        if (repoWrapper) {
                            this.putDatatype(openApiDoc_1, this.options.name, repoWrapper.repo[this.options.name], false, repoWrapper);
                            logging_1.logp("Adding datatype " + logging_1.code(this.options.name) + " from repository " + logging_1.code(repoWrapper.uri));
                        }
                        else {
                            this.putDatatype(openApiDoc_1, this.options.name);
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        logging_1.logerror("Name not found. Use -n or --name to supply a name for the datatype you wish to add, or use --source to provide a schema source file.");
                        _a.label = 5;
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        err_1 = _a.sent();
                        logging_1.logerror("Failed to add datatype: " + err_1, err_1);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DatatypeManager.prototype.updateByPrompt = function (schemas, datatype) {
        var fieldName, fieldType;
        // Get user input for field names and types:
        while (true) {
            // Name:
            fieldName = readline_sync_1.default.question(logging_1.q("Enter a field name (leave empty to finish): "));
            if (fieldName === '')
                break;
            // Type:
            var prompt_1 = function () { return readline_sync_1.default.question(logging_1.q("Enter a type name (leave blank to retrieve a list of available types): ")); };
            fieldType = prompt_1();
            while (fieldType === '') {
                this.printTypes(schemas);
                fieldType = prompt_1();
            }
            this.addField(datatype, fieldType, fieldName, schemas);
        }
    };
    DatatypeManager.prototype.addField = function (datatype, fieldType, fieldName, schemas) {
        // Add field with primitive type:
        if (ApiManager_1.jsonTypes.includes(fieldType)) {
            datatype.properties[fieldName] = { "type": fieldType };
        }
        else {
            // Add field with reference type:
            if (schemas[fieldType]) {
                datatype.properties[fieldName] = { '$ref': "#/components/schemas/" + fieldType };
            }
            else {
                logging_1.logerror("Type " + fieldType + " not found. Field " + fieldName + " not added.");
            }
        }
    };
    DatatypeManager.prototype.printTypes = function (schemas) {
        logging_1.logh1("JSON Types:");
        print_helper_1.printList(ApiManager_1.jsonTypes);
        logging_1.logh1("Schemas:");
        print_helper_1.printList(Object.keys(schemas));
    };
    DatatypeManager.prototype.update = function () {
        return __awaiter(this, void 0, void 0, function () {
            var openApiDoc, schemas, datatype, source, repoWrapper, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.apiManager.loadOpenApiJson()];
                    case 1:
                        openApiDoc = _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 6, , 7]);
                        schemas = this.apiManager.getSchemas(openApiDoc);
                        datatype = schemas[this.options.name];
                        if (!datatype)
                            throw new Error("Datatype " + this.options.name + " not found.");
                        if (!(this.options.file !== "")) return [3 /*break*/, 3];
                        source = this.options.getFileData();
                        if (!source[this.options.name])
                            throw new Error("Datatype " + this.options.name + " not found in file " + this.options.file + ".");
                        Object.assign(datatype, source[this.options.name]);
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, new RepositoryManager_1.default(this.options).findRepositoryForDatatype(this.options.name)];
                    case 4:
                        repoWrapper = _a.sent();
                        if (repoWrapper) {
                            this.putDatatype(openApiDoc, this.options.name, repoWrapper.repo[this.options.name], true, repoWrapper);
                            logging_1.logp("Updating datatype " + logging_1.code(this.options.name) + " from repository " + logging_1.code(repoWrapper.uri));
                        }
                        else {
                            this.updateByPrompt(schemas, datatype);
                        }
                        _a.label = 5;
                    case 5:
                        this.apiManager.writeApiJson(openApiDoc);
                        return [3 /*break*/, 7];
                    case 6:
                        err_2 = _a.sent();
                        logging_1.logerror("Failed to update datatype: " + err_2, err_2);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DatatypeManager.prototype.list = function () {
        var _this = this;
        this.apiManager.loadOpenApiJson()
            .then(function (openApiJson) {
            var schemas = _this.apiManager.getSchemas(openApiJson);
            if (schemas)
                print_helper_1.printList(Object.keys(schemas));
            else
                logging_1.logerror('Could not find schemas in openAPI document');
        })
            .catch(function (err) {
            logging_1.logerror("Failed to load openAPI document");
        });
    };
    DatatypeManager.prototype.datatypeInUse = function (openApiJson) {
        // \{\s*\"\$ref\"\s*\:\s*\"#\/components\/schemas\/service\"\s*\}
        var pattern = new RegExp("\\{\\s*\\\"\\$ref\\\"\\s*\\:\\s*\\\"#\\/components\\/schemas\\/" + this.options.name + "\\\"\\s*\\}");
        return JSON.stringify(openApiJson).match(pattern) !== null;
    };
    DatatypeManager.prototype.delete = function () {
        var _this = this;
        // Validate parameters:
        if (this.options.name === '') {
            logging_1.logerror("Name not found. Use -n or --name to supply a name for the datatype you wish to delete.");
            return;
        }
        // Prompt and delete:
        this.apiManager.loadOpenApiJson()
            .then(function (openApiJson) {
            var schemas = _this.apiManager.getSchemas(openApiJson);
            if (schemas[_this.options.name]) {
                var message = _this.datatypeInUse(openApiJson) ?
                    "Datatype " + _this.options.name + " is in use, do you still want to delete it? (y/n): " :
                    "Are you sure you want to delete datatype " + _this.options.name + "? (y/n): ";
                if (readline_sync_1.default.question(message) === 'y') {
                    schemas[_this.options.name] = undefined;
                    _this.apiManager.writeApiJson(openApiJson);
                    logging_1.logem("Datatype " + _this.options.name + " deleted");
                }
            }
            else {
                logging_1.logerror("Datatype " + _this.options.name + " not found.");
            }
        })
            .catch(function (err) {
            logging_1.logerror("Failed to delete datatype");
        });
    };
    return DatatypeManager;
}());
exports.default = DatatypeManager;
