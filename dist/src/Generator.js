"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Types_1 = __importDefault(require("./Types"));
var fs_1 = __importDefault(require("fs"));
var logging_1 = require("./logging");
var ApiManager_1 = __importStar(require("./ApiManager"));
var applyServiceClassTemplate_1 = __importDefault(require("./applyServiceClassTemplate"));
var applyDatatypeClassTemplate_1 = __importDefault(require("./applyDatatypeClassTemplate"));
var blackbox_services_1 = require("blackbox-services");
var applyDatabaseTemplate_1 = __importDefault(require("./applyDatabaseTemplate"));
var utils_1 = require("./utils");
var exec = require('child_process').exec;
var resolve = require('path').resolve;
// Types already defined by blackbox-services:
var ignoreTypes = [
    'link',
    'links',
    'verbose-object',
    'service',
    'rule',
    'condition',
    'value',
    'condition-type',
    'named-reference'
];
var SRC_DIR = 'src';
var GENSRC_DIR = 'gensrc';
exports.importMap = {
    Link: 'blackbox-services',
    Links: 'blackbox-services',
    NamedReference: 'blackbox-services',
    VerboseObject: 'blackbox-services',
    Service: 'blackbox-services',
    ServiceLinks: 'blackbox-services',
    ServiceLink: 'blackbox-services',
    ServiceType: 'blackbox-services',
    Rule: 'blackbox-rules',
    RuleData: 'blackbox-rules',
    Store: 'blackbox-rules',
    VariableStore: 'blackbox-rules',
    RemoteValueProtocol: 'blackbox-rules',
    Condition: 'blackbox-rules',
    ConditionImpl: 'blackbox-rules',
    ConditionData: 'blackbox-rules',
    EqualsCondition: 'blackbox-rules',
    GreaterThanCondition: 'blackbox-rules',
    LessThanCondition: 'blackbox-rules',
    AndConditio: 'blackbox-rules',
    OrConditio: 'blackbox-rules',
    Value: 'blackbox-rules',
    ValueImpl: 'blackbox-rules',
    ValueData: 'blackbox-rules',
    ConstantValue: 'blackbox-rules',
    DateTimeValue: 'blackbox-rules',
    LogValue: 'blackbox-rules',
    RemoteValue: 'blackbox-rules',
    VariableValue: 'blackbox-rules'
};
var Generator = /** @class */ (function () {
    function Generator(options) {
        this.options = options;
    }
    Generator.prototype.generate = function (type) {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!fs_1.default.existsSync(SRC_DIR))
                            fs_1.default.mkdirSync(SRC_DIR);
                        _a = type;
                        switch (_a) {
                            case Types_1.default.server: return [3 /*break*/, 1];
                            case Types_1.default.datatype: return [3 /*break*/, 3];
                            case Types_1.default.database: return [3 /*break*/, 5];
                            case Types_1.default.service: return [3 /*break*/, 7];
                            case Types_1.default.privilege: return [3 /*break*/, 9];
                        }
                        return [3 /*break*/, 11];
                    case 1: return [4 /*yield*/, this.generateServer()];
                    case 2:
                        _b.sent();
                        return [3 /*break*/, 12];
                    case 3: return [4 /*yield*/, this.generateDatatypes()];
                    case 4:
                        _b.sent();
                        return [3 /*break*/, 12];
                    case 5: return [4 /*yield*/, this.generateDatabase()];
                    case 6:
                        _b.sent();
                        return [3 /*break*/, 12];
                    case 7: return [4 /*yield*/, this.generateServices()];
                    case 8:
                        _b.sent();
                        return [3 /*break*/, 12];
                    case 9: return [4 /*yield*/, this.generatePrivileges()];
                    case 10:
                        _b.sent();
                        return [3 /*break*/, 12];
                    case 11: throw new Error("generate is not applicable to type '" + type + "'.");
                    case 12: return [2 /*return*/];
                }
            });
        });
    };
    Generator.prototype.generatePrivileges = function () {
        return __awaiter(this, void 0, void 0, function () {
            var enumName, path, oasDoc_1, paths, enumString, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!this.options.service)
                            throw new Error('Service not specified: Please specify the service for which privileges should be generated with --service (or -s).');
                        enumName = utils_1.toCammelCase(this.options.service) + 'Privileges';
                        path = this.makePath(enumName, '.ts');
                        return [4 /*yield*/, new ApiManager_1.default().loadOpenApiJson()];
                    case 1:
                        oasDoc_1 = _a.sent();
                        paths = blackbox_services_1.findChildren(oasDoc_1, this.options.service);
                        paths.push('/' + this.options.service);
                        enumString = "enum " + enumName + " {\n" +
                            paths.map(function (path) {
                                if (oasDoc_1.paths[path]) {
                                    return oasDoc_1.paths[path]; // get path values
                                }
                                else {
                                    logging_1.logwarn("WARNING: Path not found, skipping path '" + path + "'.");
                                    return null;
                                }
                            })
                                .filter(function (path) { return path; }) // remove missing paths
                                .reduce(function (methods, current) { return methods.concat(Object.values(current)); }, []) // convert to array of methods
                                .filter(function (method) { return method.operationId; }) //remove if there's no operationId
                                .map(function (method) { return "  " + method.operationId + " = '" + method.summary + "'"; }) // enum value
                                .join(',\n')
                            + ("\n}\n\nexport default " + enumName);
                        return [4 /*yield*/, fs_1.default.promises.writeFile(path, enumString)];
                    case 2:
                        _a.sent();
                        logging_1.logp("Generated privileges for " + logging_1.code(this.options.service));
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        logging_1.logerror("Failed to generate privileges: " + err_1, err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Generator.prototype.makePath = function (name, extension) {
        return (this.options.dest ? this.options.dest : SRC_DIR) + '/' + name + extension;
    };
    Generator.prototype.generateDatabase = function () {
        return __awaiter(this, void 0, void 0, function () {
            var className, type, data, path, apiManager, packageJson, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        className = utils_1.toCammelCase(this.options.datatype, true);
                        type = this.options.datatype;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        if (this.options.datatype === '')
                            throw new Error('Datatype not specified: Use -d or --datatype to specify the datatype for which the database should be created.');
                        data = applyDatabaseTemplate_1.default(className, type);
                        path = this.makePath(className + 'Database', '.ts');
                        return [4 /*yield*/, fs_1.default.promises.writeFile(path, data)];
                    case 2:
                        _a.sent();
                        apiManager = new ApiManager_1.default();
                        return [4 /*yield*/, apiManager.loadPackageJson()];
                    case 3:
                        packageJson = _a.sent();
                        if (!packageJson.dependencies) {
                            packageJson.dependencies = {};
                        }
                        if (!packageJson.dependencies['blackbox-database']) {
                            packageJson.dependencies['blackbox-database'] = 'https://ellipsistechnology@bitbucket.org/ellipsistechnology/blackbox-database.git';
                            apiManager.writePackageJson(packageJson);
                        }
                        logging_1.logp("Generated database for " + logging_1.code(this.options.datatype) + ": " + path);
                        return [3 /*break*/, 5];
                    case 4:
                        err_2 = _a.sent();
                        logging_1.logerror("Failed to generate database: " + err_2, err_2);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Generator.prototype.generateDatatypes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var oasDoc_2, schemaToClass_1, datatypesJS_1, err_3;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, new ApiManager_1.default().loadOpenApiJson()];
                    case 1:
                        oasDoc_2 = _a.sent();
                        if (!oasDoc_2.components || !oasDoc_2.components.schemas)
                            throw new Error("/components/schemas not found in Open API Document");
                        schemaToClass_1 = this.schemaToClassMap(oasDoc_2);
                        datatypesJS_1 = {};
                        Object.keys(oasDoc_2.components.schemas).forEach(function (name) {
                            if (!ignoreTypes.includes(name)) {
                                var schema_1 = oasDoc_2.components.schemas[name];
                                if (schema_1.type === 'object' && schema_1.properties) {
                                    var templateData = {
                                        name: name,
                                        properties: Object.keys(schema_1.properties).map(function (prop) { return ({
                                            type: (schema_1.properties[prop]['$ref'] ? schemaToClass_1[schema_1.properties[prop]['$ref']] :
                                                schema_1.properties[prop].type === 'array' && schema_1.properties[prop].items && schema_1.properties[prop].items['$ref'] ? schemaToClass_1[schema_1.properties[prop].items['$ref']] + '[]' :
                                                    schema_1.properties[prop].type === 'array' && schema_1.properties[prop].items && ApiManager_1.jsonTypes.includes(schema_1.properties[prop].items.type) ? schema_1.properties[prop].items.type + '[]' :
                                                        ApiManager_1.jsonTypes.includes(schema_1.properties[prop].type) ? schema_1.properties[prop].type :
                                                            Array.isArray(schema_1.properties[prop].oneOf) ? (schema_1.properties[prop].oneOf
                                                                .map(function (type) { return (ApiManager_1.jsonTypes.includes(type.type) ? type.type :
                                                                type['$ref'] ? schemaToClass_1[type['$ref']] :
                                                                    ''); })
                                                                .filter(function (type) { return type; })
                                                                .join('|')) :
                                                                'any'),
                                            name: prop
                                        }); }),
                                        required: Array.isArray(schema_1.required) ? schema_1.required : []
                                    };
                                    // TODO all special cases above need to be below and vise-versa; i.e. extract to methods
                                    datatypesJS_1[templateData.name] = applyDatatypeClassTemplate_1.default(templateData);
                                }
                                else if (schema_1.type === 'string' && (schema_1.pattern || schema_1.format)) {
                                    datatypesJS_1[name] = applyDatatypeClassTemplate_1.default({
                                        name: name,
                                        properties: [],
                                        parentClasses: ['String'],
                                        required: Array.isArray(schema_1.required) ? schema_1.required : []
                                    });
                                }
                                else if (schema_1.type === 'string' && (Array.isArray(schema_1.enum))) {
                                    datatypesJS_1[name] = applyDatatypeClassTemplate_1.default({
                                        name: name,
                                        properties: [],
                                        values: schema_1.enum,
                                        required: Array.isArray(schema_1.required) ? schema_1.required : []
                                    });
                                }
                                else if (Array.isArray(schema_1.oneOf)) {
                                    logging_1.logwarn("#/components/schemas/" + name + ": oneOf is not supported");
                                    // datatypesJS[name] = applyDatatypeClassTemplate({
                                    //   name: name,
                                    //   properties: [],
                                    //   parentClasses: schema.oneOf.map((type:any) => (
                                    //     type['$ref'] ? schemaToClass[type['$ref']] :
                                    //     type.type && (<any>jsonTypes).includes(type.type) ? (<string>type.type).charAt(0).toUpperCase()+(<string>type.type).substring(1) :
                                    //     ''
                                    //   )).filter((type:any) => type)
                                    // })
                                }
                                else if (Array.isArray(schema_1.allOf)) {
                                    datatypesJS_1[name] = applyDatatypeClassTemplate_1.default({
                                        name: name,
                                        properties: [],
                                        parentClasses: schema_1.allOf.map(function (type) { return type['$ref'] ? schemaToClass_1[type['$ref']] : ''; }).filter(function (type) { return type && !ApiManager_1.jsonTypes.includes(type); }),
                                        required: Array.isArray(schema_1.required) ? schema_1.required : []
                                    });
                                }
                                // else if(Array.isArray(schema.anyOf)) {
                                //
                                // }
                            }
                        });
                        Object.keys(datatypesJS_1).forEach(function (name) { return __awaiter(_this, void 0, void 0, function () {
                            var className, path;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        className = name.charAt(0).toUpperCase() + name.substring(1);
                                        path = this.makePath(utils_1.toCammelCase(className), '.ts');
                                        if (!(!path.match("^(/)?" + GENSRC_DIR) && fs_1.default.existsSync(path))) return [3 /*break*/, 1];
                                        logging_1.logwarn(path + " already exists: Skipping datatype " + logging_1.code(className));
                                        return [3 /*break*/, 3];
                                    case 1: return [4 /*yield*/, fs_1.default.promises.writeFile(path, datatypesJS_1[name])];
                                    case 2:
                                        _a.sent();
                                        logging_1.logp("Generated datatype " + logging_1.code(className));
                                        _a.label = 3;
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); });
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        logging_1.logerror("Failed to generate datatypes: " + err_3, err_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Generator.prototype.generateServices = function () {
        return __awaiter(this, void 0, void 0, function () {
            var oasDoc_3, servicesJS_1, schemaToClass_2, err_4;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, new ApiManager_1.default().loadOpenApiJson()];
                    case 1:
                        oasDoc_3 = _a.sent();
                        if (!oasDoc_3.paths)
                            throw new Error("/path not found in Open API Document");
                        servicesJS_1 = {};
                        schemaToClass_2 = this.schemaToClassMap(oasDoc_3);
                        blackbox_services_1.findParentPaths(oasDoc_3).forEach(function (parent) {
                            if (parent !== '/rulebase') {
                                var paths = Object.keys(oasDoc_3.paths).filter(function (path) { return path.startsWith(parent); });
                                var templateData_1 = {
                                    name: parent.replace(/\//, ''),
                                    methods: []
                                };
                                paths.forEach(function (path) {
                                    Object.keys(oasDoc_3.paths[path]).forEach(function (method) {
                                        var operationId = oasDoc_3.paths[path][method].operationId;
                                        if (operationId) {
                                            templateData_1.methods.push({
                                                type: _this.methodType(oasDoc_3, path, method, schemaToClass_2),
                                                operationId: utils_1.toCammelCase(operationId)
                                            });
                                        }
                                    });
                                });
                                servicesJS_1[templateData_1.name] = applyServiceClassTemplate_1.default(templateData_1);
                            }
                        });
                        Object.keys(servicesJS_1).forEach(function (name) { return __awaiter(_this, void 0, void 0, function () {
                            var className, path;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        className = utils_1.toCammelCase(name.charAt(0).toUpperCase() + name.substring(1) + 'ServiceImpl');
                                        path = this.makePath(className, '.ts');
                                        if (!(!path.match("^(/)?" + GENSRC_DIR) && fs_1.default.existsSync(path))) return [3 /*break*/, 1];
                                        logging_1.logwarn(path + " already exists: Skipping service " + logging_1.code(className));
                                        return [3 /*break*/, 3];
                                    case 1: return [4 /*yield*/, fs_1.default.promises.writeFile(path, servicesJS_1[name])];
                                    case 2:
                                        _a.sent();
                                        logging_1.logp("Generated service class " + logging_1.code(className));
                                        _a.label = 3;
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); });
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        logging_1.logerror("Failed to generate services: " + err_4, err_4);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Generator.prototype.schemaToClassMap = function (oasDoc) {
        return Object.keys(oasDoc.components.schemas).reduce(function (map, schemaName) {
            // Remove non-words (e.g. hyphens):
            var tokens = schemaName.match(/\w+/g);
            if (!tokens)
                throw new Error("Invalid schema found '" + schemaName + "'");
            // Convert to camel-case:
            map["#/components/schemas/" + schemaName] = tokens.reduce(function (className, t) { return className + t.charAt(0).toUpperCase() + t.substring(1); }, '');
            return map;
        }, {});
    };
    Generator.prototype.methodType = function (oasDoc, path, method, schemaToClass) {
        var schema = oasDoc.paths[path][method].responses['200'].content['application/json'].schema;
        var allOf;
        if (Array.isArray(schema.allOf))
            allOf = schema.allOf;
        else if (schema.type === 'array' && schema.items && schema.items.allOf)
            allOf = schema.items.allOf;
        var type = '';
        if (allOf) {
            type = allOf
                .map(function (childSchema) { return schemaToClass[childSchema['$ref']]; })
                .filter(function (type) { return type !== 'VerboseObject'; }) // by default this is handled by the service caller; see /gensrc/service
                .join('&');
        }
        else if (schema.type === 'array' && schema.items && schema.items['$ref']) {
            type = schemaToClass[schema['$ref']];
        }
        else if (schema['$ref']) {
            type = schemaToClass[schema['$ref']];
        }
        if (type && schema.type === 'array')
            return type + '[]';
        else
            return type;
    };
    Generator.prototype.generateServer = function () {
        return __awaiter(this, void 0, void 0, function () {
            var binPath, JAVA_OPTS, dest, source, command, cmd;
            return __generator(this, function (_a) {
                binPath = resolve(__dirname, 'openapi-generator-cli.jar');
                JAVA_OPTS = process.env['JAVA_OPTS'] || '';
                dest = this.options.dest ? this.options.dest : '.';
                source = this.options.file ? this.options.file : 'openapi.json';
                command = "java " + JAVA_OPTS + " -jar \"" + binPath + "\" generate -g blackbox-nodejs-server -i " + source + " -o " + dest;
                cmd = exec(command);
                cmd.stdout.pipe(process.stdout);
                cmd.stderr.pipe(process.stderr);
                cmd.on('exit', process.exit);
                return [2 /*return*/];
            });
        });
    };
    return Generator;
}());
exports.default = Generator;
