"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var commander_1 = __importDefault(require("commander"));
var print_helper_1 = require("./print-helper");
var fs_1 = __importDefault(require("fs"));
var logging_1 = require("./logging");
var CliOptions = /** @class */ (function () {
    function CliOptions(args) {
        var _this = this;
        this.name = "";
        this.title = "New Blackbox API";
        this.description = "";
        this.service = "";
        this.datatype = "";
        this.methods = "post,put,patch,delete";
        this.all = false;
        this.summary = "";
        this.file = "";
        this.url = "";
        this.dest = "";
        this.verbose = false;
        this.singular = "";
        commander_1.default
            .description("The Blackbox CLI")
            .usage('bb <command> <type> [options]')
            .option('-n, --name <name>', "The name of the entity being added or modified.")
            .option('-t, --title <title>', "The title of the API.")
            .option('--desc <description>', 'The description of the API.')
            .option('-s, --service <service>', "The parent service in the case of sub-services. Defaults to no parent; i.e. a root level service.")
            .option('--singular <singular>', "The singular form of the service name when using plural service paths. Used for the operationId.")
            .option('-d, --datatype <datatype>', "The data type of a service.")
            .option('-m, --methods <methods>', "The HTTP methods to be supported by the service.")
            .option('-a, --all', "Apply operation to all relevant items. E.g. bb delete service -a -n s1 will remove service s1 and any child services.")
            .option('-f, --file <file>', "A source file to read in data appropriate to the command and type.")
            .option('-u, --url <url>', "A source url to read in data appropriate to the command and type.")
            .option('--dest <dest>', "Destination path for generated files.")
            .option('--summary <summary>', "Summary for a service.")
            .option('-v, --verbose', "Display verbose output.")
            .parse(args);
        // console.log(JSON.stringify(program, null, 2))
        Object.keys(commander_1.default.opts()).forEach(function (opt) {
            if (commander_1.default[opt] !== undefined && typeof commander_1.default[opt] === 'string') {
                _this[opt] = commander_1.default[opt];
            }
        });
        if (commander_1.default.verbose)
            console.log("Verbose logging enabled");
        logging_1.setVerboseLogging(commander_1.default.verbose);
    }
    CliOptions.prototype.servicePath = function () {
        return this.service === '' ? "/" + this.name : "/" + this.service + "/" + this.name;
    };
    CliOptions.prototype.serviceTag = function () {
        return this.service === '' ? this.name : this.service;
    };
    CliOptions.prototype.describeOptions = function (prefix, format) {
        if (format === void 0) { format = print_helper_1.defaultFormat; }
        var colWidth = print_helper_1.maxLength(commander_1.default.options.map(function (opt) { return opt.flags; }));
        return commander_1.default.options.map(function (option) { return (format(prefix, print_helper_1.fixWidth(option.flags, colWidth), option.description)); }).reduce(function (accumulator, currentValue) { return accumulator + currentValue + '\n'; }, '');
    };
    CliOptions.prototype.methodsSet = function () {
        return this.methods.split(",");
    };
    CliOptions.prototype.getFileData = function () {
        if (!this.file)
            return undefined;
        if (!this.sourceData) {
            var strData = fs_1.default.readFileSync(this.file);
            this.sourceData = JSON.parse(strData.toString());
        }
        return this.sourceData;
    };
    return CliOptions;
}());
exports.default = CliOptions;
