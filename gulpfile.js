const { parallel, series, src, dest } = require('gulp');
const ts = require('gulp-typescript');
const chmod = require('gulp-chmod');
const fs = require('fs')

var tsProject = ts.createProject('tsconfig.json');

function copyLib() {
  return src('lib/*')
    .pipe(dest('dist/'));
}

function copyTemplates() {
  return src('src/templates/*')
    .pipe(dest('dist/templates/'));
}

function makeExecutable() {
  return src('dist/index.js')
        .pipe(chmod(0o755))
        .pipe(dest('dist'))
}

function build() {
  var tsResult = tsProject.src()
      .pipe(tsProject());

  return tsResult.pipe(dest('dist'));
}

exports.default = parallel(
  series(build, makeExecutable),
  copyLib,
  copyTemplates
)
