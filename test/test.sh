../dist/index.js init -v -n weather -t "A weather API." --desc \
"An API for retrieving temperature and humidity information." && \
../dist/index.js add repository -v --url https://bitbucket.org/ellipsistechnology/blackbox-datatypes/raw/691588c69282a4e5d8c5a6cd6a042acb18f8abb2/types.json?raw && \
../dist/index.js add service -v -n weather --summary \
"Weather service: Provides temperature and humidity information." && \
../dist/index.js add service -v -n temperature -s weather --summary \
"Returns a temperature object." && \
../dist/index.js add datatype -v -n temperature -f types.json && \
../dist/index.js add datatype -v -n user -f types.json && \
../dist/index.js update service -v -s weather -n temperature -d temperature -m "" && \
../dist/index.js add service -v -s weather -n humidity -d number -m "" && \
../dist/index.js generate service -v  && \
../dist/index.js generate datatype -v  && \
../dist/index.js generate server -v  && \
npm i && \
npm run build #&& \
#npm run start
