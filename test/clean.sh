rm -f -r .openapi-generator && \
rm -f -r dist && \
rm -f -r src && \
rm -f -r gensrc && \
rm -f -r node_modules && \
rm -f .openapi-generator-ignore && \
rm -f blackbox.json && \
rm -f *.js && \
rm -f *.ts && \
rm -f openapi.json && \
rm -f package*.json && \
rm -f README.md && \
rm -f tsconfig.json
