import CliOptions from './CliOptions'
import ApiManager, {jsonTypes, OPENAPI_FILENAME} from './ApiManager'
import {logerror, logq, q, logcode, logh1, logem, logp, code} from './logging'
import readlineSync from 'readline-sync'
import {printList} from './print-helper'
import RepositoryManager, { RepoWrapper } from './RepositoryManager';

// TODO allow add from repository

export default class DatatypeManager {
  options:CliOptions;
  apiManager:ApiManager;

  constructor(options:CliOptions) {
    this.options = options
    this.apiManager = new ApiManager();
  }

  private deepCopy(src:any):any {
    let copy:any
    if(Array.isArray(src)) {
      copy = (<[]>src).map(entry => this.deepCopy(entry))
    }
    else if((<any>jsonTypes).includes(typeof src)) {
      copy = src
    }
    else {
      copy = {}
      Object.keys(src).forEach((key:string) => {
        copy[key] = this.deepCopy(src[key])
      })
    }
    return copy
  }

  private allRefs(src:any[]):any[] {
    return src.filter((entry:any) => entry["$ref"] !== undefined)
  }

  addDependantDatatypes(openApiJson:any, repoWrapper:RepoWrapper, name:string) {

    const add = (name:string) => {
      const copy = this.deepCopy(repoWrapper.repo[name])
      let toAdd:string[] = []
      let refNodes:any[] = []

      // Find any $refs in oneOf, anyOf, allOf:
      if(Array.isArray(copy.oneOf)) {
        refNodes = this.allRefs(<string[]>copy.oneOf)
      }
      else if(Array.isArray(copy.allOf)) {
        refNodes = this.allRefs(<string[]>copy.allOf)
      }
      else if(Array.isArray(copy.anyOf)) {
        refNodes = this.allRefs(<string[]>copy.anyOf)
      }
      else if(copy.properties) {

        // Find children that are also references within the repository:
        Object.keys(copy.properties).forEach((key:string) => {

          // Property is a $ref:
          if(copy.properties[key]["$ref"]) {
            refNodes.push(copy.properties[key])
          }

          // Property is an array with items that are $ref:
          else if(copy.properties[key].type
            && copy.properties[key].type === "array"
            && copy.properties[key].items
            && copy.properties[key].items["$ref"]) {
              refNodes.push(copy.properties[key].items)
          }

          // Property includes $refs in oneOf, anyOf, allOf:
          else if(Array.isArray(copy.properties[key].oneOf)) {
            refNodes = refNodes.concat(this.allRefs(<string[]>copy.properties[key].oneOf))
          }
          else if(Array.isArray(copy.properties[key].anyOf)) {
            refNodes = refNodes.concat(this.allRefs(<string[]>copy.properties[key].anyOf))
          }
          else if(Array.isArray(copy.properties[key].allOf)) {
            refNodes = refNodes.concat(this.allRefs(<string[]>copy.properties[key].allOf))
          }

        })
      }

      // Update $ref:

      refNodes.forEach((n) => {
        const value:string = n["$ref"]
        let newRef = /(?<=^#\/)[^\/]+$/.exec(value) // Strip out #/
        if(newRef) {
          n["$ref"] = `#/components/schemas/${newRef[0]}`
          toAdd.push(newRef[0])
        }
        else {
          throw new Error(`Failed to parse path ${value} in repository: Note that all $refs must start with #/ and be relative to the root of the repository.`)
        }
      })

      // Recurse on children:
      toAdd.forEach(add)

      // Add the datatype to openapi.json:
      openApiJson.components.schemas[name] = copy
      logp(`Added datatype ${code(name)} to ${code(OPENAPI_FILENAME)} from repository.`)

    }
    add(name)
  }

  private mapRefs(datatype: any) {
    if(datatype.properties) {
      Object.values(datatype.properties).forEach((prop: any) => {
        if(typeof prop['$ref'] === 'string') {
          prop['$ref'] = (<string>prop['$ref']).replace('#/', '#/components/schemas/')
        } else if(prop.type === 'array' && prop.items && typeof prop.items['$ref'] === 'string') {
          prop.items['$ref'] = (<string>prop.items['$ref']).replace('#/', '#/components/schemas/')
        }
      })
    }
  }

  private putDatatype(openApiDoc:any, name:string, datatype:any=undefined, allowReplace=false, repoWrapper:RepoWrapper|undefined=undefined) {
    let schemas = this.apiManager.getSchemas(openApiDoc);
    if(!allowReplace && schemas[this.options.name]) {
      logerror(`Datatype with name ${name} already exists; Datatype not added.`)
      return
    }

    if(!datatype) {
      datatype = {"type": "object", "properties": {}}
  		this.updateByPrompt(schemas, datatype)
    }

    if(repoWrapper) {
      this.addDependantDatatypes(openApiDoc, repoWrapper, this.options.name)
    } else {
      this.mapRefs(datatype)
      schemas[name] = datatype
    }


    this.apiManager.writeApiJson(openApiDoc)
    logem(`Successfully ${allowReplace?'updated':'added'} datatype ${name}`)
  }

  async add() {
    try {
      const openApiDoc = await this.apiManager.loadOpenApiJson()

      // If source file is provided then read from that:
      if(this.options.file !== "") {
        const source = this.options.getFileData()

        // No name means add all:
        if(this.options.name === '')
          Object.keys(source).forEach( name => this.putDatatype(openApiDoc, name, source[name]) )
        // Otherwise just add the type with the given name:
        else
          this.putDatatype(openApiDoc, this.options.name, source[this.options.name])
      }
      else if(this.options.name !== '') {
        const repoWrapper = await new RepositoryManager(this.options).findRepositoryForDatatype(this.options.name)
        if(repoWrapper) {
          this.putDatatype(openApiDoc, this.options.name, repoWrapper.repo[this.options.name], false, repoWrapper)
          logp(`Adding datatype ${code(this.options.name)} from repository ${code(repoWrapper.uri)}`)
        }
        else {
          this.putDatatype(openApiDoc, this.options.name)
        }
      }
      else {
        logerror("Name not found. Use -n or --name to supply a name for the datatype you wish to add, or use --source to provide a schema source file.")
      }
    }
    catch(err) {
      logerror(`Failed to add datatype: ${err}`, err)
    }
  }

  private updateByPrompt(schemas:any, datatype:any) {
		let fieldName:string, fieldType:string;

		// Get user input for field names and types:
		while(true) {

			// Name:
			fieldName = readlineSync.question(q("Enter a field name (leave empty to finish): "));
			if(fieldName === '')
				break;

			// Type:
      const prompt = () => readlineSync.question(q("Enter a type name (leave blank to retrieve a list of available types): "))
			fieldType = prompt()
			while(fieldType === '') {
				this.printTypes(schemas);
				fieldType = prompt()
			}

			this.addField(datatype, fieldType, fieldName, schemas);
		}
	}

  private addField(datatype: any, fieldType: string, fieldName: string, schemas: any) {

  	// Add field with primitive type:
    if ((<any>jsonTypes).includes(fieldType)) {
      datatype.properties[fieldName] = { "type": fieldType };
    }
    else {
      // Add field with reference type:
      if (schemas[fieldType]) {
        datatype.properties[fieldName] = { '$ref': `#/components/schemas/${fieldType}` };
      }
      else {
        logerror("Type " + fieldType + " not found. Field " + fieldName + " not added.");
      }
    }
  }

  printTypes(schemas:any) {
		logh1("JSON Types:")
    printList(jsonTypes)

		logh1("Schemas:")
    printList(Object.keys(schemas))
	}

  async update() {
    const openApiDoc = await this.apiManager.loadOpenApiJson()
    try {
      let schemas = this.apiManager.getSchemas(openApiDoc)
  		let datatype = schemas[this.options.name]
  		if(!datatype)
        throw new Error("Datatype "+this.options.name+" not found.")

      if(this.options.file !== "") {
        const source = this.options.getFileData()
        if(!source[this.options.name])
          throw new Error(`Datatype ${this.options.name} not found in file ${this.options.file}.`)
        Object.assign(datatype, source[this.options.name])
      }
      else {
        const repoWrapper = await new RepositoryManager(this.options).findRepositoryForDatatype(this.options.name)
        if(repoWrapper) {
          this.putDatatype(openApiDoc, this.options.name, repoWrapper.repo[this.options.name], true, repoWrapper)
          logp(`Updating datatype ${code(this.options.name)} from repository ${code(repoWrapper.uri)}`)
        }
        else {
          this.updateByPrompt(schemas, datatype)
        }
      }

      this.apiManager.writeApiJson(openApiDoc);
    }
    catch(err) {
      logerror(`Failed to update datatype: ${err}`, err)
    }
  }

  list() {
    this.apiManager.loadOpenApiJson()
    .then( (openApiJson) => {
      let schemas = this.apiManager.getSchemas(openApiJson)
      if(schemas)
        printList(Object.keys(schemas))
      else
        logerror('Could not find schemas in openAPI document')
    })
    .catch( (err) => {
      logerror(`Failed to load openAPI document`)
    })
  }

  datatypeInUse(openApiJson:any):boolean {
		// \{\s*\"\$ref\"\s*\:\s*\"#\/components\/schemas\/service\"\s*\}
    const pattern = new RegExp("\\{\\s*\\\"\\$ref\\\"\\s*\\:\\s*\\\"#\\/components\\/schemas\\/"+this.options.name+"\\\"\\s*\\}")
    return JSON.stringify(openApiJson).match(pattern) !== null
	}

  delete() {
    // Validate parameters:
    if(this.options.name === '') {
      logerror("Name not found. Use -n or --name to supply a name for the datatype you wish to delete.")
      return
    }

    // Prompt and delete:
    this.apiManager.loadOpenApiJson()
    .then( (openApiJson) => {
      let schemas = this.apiManager.getSchemas(openApiJson)
      if(schemas[this.options.name]) {
        const message = this.datatypeInUse(openApiJson) ?
            "Datatype "+this.options.name+" is in use, do you still want to delete it? (y/n): " :
            "Are you sure you want to delete datatype "+this.options.name+"? (y/n): ";
        if(readlineSync.question(message) === 'y') {
          schemas[this.options.name] = undefined
          this.apiManager.writeApiJson(openApiJson)
          logem(`Datatype ${this.options.name} deleted`)
        }
      }
      else {
        logerror("Datatype "+this.options.name+" not found.");
      }
    })
    .catch( (err) => {
      logerror(`Failed to delete datatype`)
    })
  }
}
