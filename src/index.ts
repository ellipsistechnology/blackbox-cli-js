#!/usr/bin/env node

import CliApplication from './CliApplication'

async function main() {
  try {
    await new CliApplication(process.argv).run()
  }
  catch(err) {
    if (typeof (err) === 'string') {
        console.error(err)
    }
    else if (err instanceof Error) {
      console.error(err.message)
    }
    else {
      console.log(err)
    }
  }
}

main()

// program
//   .version('0.0.1')
//   .description("The Blackbox CLI for creating ")
//   .usage('bb <command> <type> [options]')
//   .option('-n, --name <name>', 'The entities name.')
//   .parse(process.argv);
