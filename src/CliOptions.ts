import program from 'commander';
import {Option} from 'commander'
import {defaultFormat, fixWidth, maxLength} from './print-helper'
import fs from 'fs'
import { setVerboseLogging } from './logging';

export default class CliOptions {
  name:string = "";
  title:string = "New Blackbox API";
  description:string = "";
  service:string = "";
  datatype:string = "";
  methods:string = "post,put,patch,delete";
  all:boolean = false;
  summary:String = "";
  file:string = "";
  url:string = "";
  dest:string = "";
  verbose:boolean = false;
  singular:string = "";
  sourceData:string | undefined

  constructor(args:string[]) {
    program
      .description("The Blackbox CLI")
      .usage('bb <command> <type> [options]')
    	.option('-n, --name <name>', "The name of the entity being added or modified.")
    	.option('-t, --title <title>', "The title of the API.")
    	.option('--desc <description>', 'The description of the API.')
      .option('-s, --service <service>', "The parent service in the case of sub-services. Defaults to no parent; i.e. a root level service.")
      .option('--singular <singular>', "The singular form of the service name when using plural service paths. Used for the operationId.")
    	.option('-d, --datatype <datatype>', "The data type of a service.")
    	.option('-m, --methods <methods>', "The HTTP methods to be supported by the service.")
    	.option('-a, --all', "Apply operation to all relevant items. E.g. bb delete service -a -n s1 will remove service s1 and any child services.")
      .option('-f, --file <file>', "A source file to read in data appropriate to the command and type.")
      .option('-u, --url <url>', "A source url to read in data appropriate to the command and type.")
      .option('--dest <dest>', "Destination path for generated files.")
    	.option('--summary <summary>', "Summary for a service.")
      .option('-v, --verbose', "Display verbose output.")
      .parse(args);

// console.log(JSON.stringify(program, null, 2))
    Object.keys(program.opts()).forEach( (opt:string) => {
      if(program[opt] !== undefined && typeof program[opt] === 'string') {
        (<any>this)[opt] = program[opt]
      }
    })

    if(program.verbose)
      console.log(`Verbose logging enabled`)
    setVerboseLogging(program.verbose)
	}

  servicePath():string {
    return this.service === '' ? "/"+this.name : "/"+this.service+"/"+this.name
  }

  serviceTag():string {
    return this.service === '' ? this.name : this.service
  }

  describeOptions(prefix:string, format = defaultFormat):string {
    const colWidth = maxLength( program.options.map((opt:Option) => opt.flags) )

    return program.options.map( (option:Option) => (
      format(prefix, fixWidth(option.flags, colWidth), option.description)
    ) ).reduce((accumulator:string, currentValue:string) => accumulator + currentValue + '\n', '')
  }

  methodsSet():string[] {
		return this.methods.split(",")
	}

  getFileData():any {
    if(!this.file)
      return undefined

    if(!this.sourceData) {
      const strData = fs.readFileSync(this.file)
      this.sourceData = JSON.parse(strData.toString())
    }

    return this.sourceData
  }
}
