export default function applyDatabaseTemplate(className: string, type: string) {
    return `import {database} from 'blackbox-database'
import ${className} from './${className}';
import {named} from 'blackbox-ioc';

@database()
@named('${type}-database')
export default class ${className}Database {
  async get${className}(_name:string): Promise<${className}> {return <${className}>{}}
  async get${className}List(): Promise<${className}[]> {return <${className}[]>[]}
  async create${className}(_${type.replace('-', '')}: ${className}) {}
  async update${className}(_name: string, _${type.replace('-', '')}: ${className}) {}
  async delete${className}(_name: string) {}
}
`
}
