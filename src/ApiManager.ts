import BlackboxConfig from './BlackboxConfig'
import fs from 'fs'
import {logerror} from './logging'

export const OPENAPI_FILENAME: string = "openapi.json"
export const CONFIG_FILENAME: string = "blackbox.json"
export const PACKAGE_FILENAME: string = "package.json"
export const jsonTypes:string[] = [
  "string",
  "integer",
  "number",
  "boolean",
  "array",
  "object"
]

export default class ApiManager {
	constructor() {
	}

  loadConfigJson(): Promise<any> {
    return new Promise<any>( (resolve, reject) => {
      fs.readFile(CONFIG_FILENAME, (err:any, data:any) => {
        if(err) {
          logerror("Error reading Blackbox config in file "+CONFIG_FILENAME
            +". Have you initialised your Blackbox api: bb init")
          reject(err)
        }
        else {
          resolve(JSON.parse(data))
        }
      })
    })
  }

	loadOpenApiJson():Promise<any> {
    return new Promise( (resolve, reject) => {
      fs.readFile(OPENAPI_FILENAME, (err:any, data:any) => {
        if(err) {
          logerror("Error reading OpenAPI document in file "+OPENAPI_FILENAME
            +". Have you initialised your Blackbox api: bb init")
          reject(err)
        }
        else {
          resolve(JSON.parse(data))
        }
      })
    })
	}

  async loadPackageJson(): Promise<any> {
    return new Promise<any>( (resolve, reject) => {
      fs.readFile(PACKAGE_FILENAME, (err:any, data:any) => {
        if(err) {
          logerror("Error reading "+PACKAGE_FILENAME)
          reject(err)
        }
        else {
          resolve(JSON.parse(data))
        }
      })
    })
  }

	// public String loadOpenApiJsonAsString() {
	// 	try {
	// 		return Files.lines(new File(OPENAPI_FILENAME).toPath()).collect(Collectors.joining("\n"));
	// 	} catch (IOException e) {
	// 		logger.severe("Error reading OpenAPI document in file "+OPENAPI_FILENAME+". Have you initialised your Blackbox api: bb init");
	// 		if(config.verbose)
	// 			throw new RuntimeException(e);
	// 		else
	// 			return null;
	// 	}
	// }

	writeApiJson(json:any) {
    fs.writeFile(OPENAPI_FILENAME, JSON.stringify(json, null, 2), (err) => {
      if(err)
        console.error(`Failed to write to ${OPENAPI_FILENAME}: ${err}`)
    })
	}

  writeConfigJson(json:any) {
    fs.writeFile(CONFIG_FILENAME, JSON.stringify(json, null, 2), (err) => {
      if(err)
        console.error(`Failed to write to ${CONFIG_FILENAME}: ${err}`)
    })
	}

  writePackageJson(json: any) {
    fs.writeFile(PACKAGE_FILENAME, JSON.stringify(json, null, 2), (err) => {
      if(err)
        console.error(`Failed to write to ${PACKAGE_FILENAME}: ${err}`)
    })
  }

	// public JsonElement newJsonObject(String json) {
	// 	JsonParser parser = new JsonParser();
	// 	return parser.parse(json);
	// }
  //
	// public JsonElement newJsonObject(File file) {
	// 	JsonParser parser = new JsonParser();
	// 	try {
	// 		return parser.parse(new FileReader(file));
	// 	} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
	// 		logger.severe("Error reading json file "+file.getPath());
	// 		if(config.verbose)
	// 			throw new RuntimeException(e);
	// 		else
	// 			return null;
	// 	}
	// }
  //
	// public JsonElement getByPath(JsonObject json, String[] path) {
	// 	for (String node : path) {
	// 		JsonElement element = json.get(node);
	// 		if(element != null) {
	// 			json = element.getAsJsonObject();
	// 		}
	// 		else {
	// 			logger.severe("Path not found: "+node);
	// 			for (String p : path) {
	// 				logger.severe(p);
	// 			}
	// 			throw new NullPointerException(node);
	// 		}
	// 	}
	// 	return json;
	// }
  //
	// public JsonElement getOrCreateJsonElement(JsonElement parent, String memberName, Supplier<JsonElement> creator) {
	// 	JsonElement member = parent.getAsJsonObject().get(memberName);
	// 	if(member == null) {
	// 		member = creator.get();
	// 		parent.getAsJsonObject().add(memberName, member);
	// 	}
	// 	return member;
	// }
  //
	getSchemas(json:any):any {
		return json.components.schemas
	}

	getSchemaByPath(openApiRoot:any, path:string):any {
		return this.getSchemaParentByPath(openApiRoot, path).schema
	}

	getSchemaParentByPath(openApiRoot:any, path:string):any {
		return openApiRoot.paths[path].get.responses['200'].content['application/json']
	}

	// public JsonElement newJsonObject(InputStream stream) {
	// 	JsonParser parser = new JsonParser();
	// 	return parser.parse(new InputStreamReader(stream));
	// }
  //
	// public JsonElement newJsonObject(InputStream stream, Map<String, String> templateMap) {
  //
	// 	try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
  //
	// 		String jsonString = reader.lines()
	// 				.map(replaceValues(templateMap))
	// 				.collect(Collectors.joining("\n"));
  //
	// 		JsonParser parser = new JsonParser();
	// 		return parser.parse(jsonString.toString());
  //
	// 	} catch (IOException e) {
	// 		logger.severe("Error loading JSON from stream.");
	// 		if(config.verbose)
	// 			throw new RuntimeException(e);
	// 		else
	// 			return null;
	// 	}
	// }
  //
	// public Function<? super String, ? extends String> replaceValues(Map<String, String> templateMap) {
	// 	return (line) -> {
	// 		for (Entry<String, String> entry : templateMap.entrySet()) {
	// 			line = line.replaceAll("\\$\\{"+entry.getKey()+"\\}", entry.getValue());
	// 		}
	// 		return line;
	// 	};
	// }
}
