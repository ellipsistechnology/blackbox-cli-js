import CliOptions from './CliOptions'
import fs from 'fs';
import appConfig from './appConfig'
import ApiManager from './ApiManager'

import openApiTemplate from './templates/openapi-template.json'
import {CONFIG_FILENAME, OPENAPI_FILENAME} from './ApiManager'

import {logh1, logcode, logem, logp, logerror, em, p, code, file} from './logging'

// const OPENAPI_TEMPLATE_FILENAME:string = "openapi-template.json";

export default class Initialiser {
  options:CliOptions

  constructor(opts:CliOptions) {
    this.options = opts
  }

  init() {
    // Validate options:
    if(this.options.name === '') {
      logerror("Name not found. Use -n or --name to supply a name for the API.")
      return
    }
    if(fs.existsSync(CONFIG_FILENAME)) {
      logerror(`${CONFIG_FILENAME} file already exists. Has your project already been initialised?`)
      return
    }
    if(fs.existsSync(OPENAPI_FILENAME)) {
      logerror(`${OPENAPI_FILENAME} file already exists. Has your project already been initialised?`)
      return
    }

    // Setup config files:
    if(this.initBlackbox() && this.initOpenAPI()) {
      logem(`Blackbox project successfully initialised.`)
    }
  }

  initBlackbox():boolean {
    try {
      fs.writeFileSync(
        CONFIG_FILENAME,
`\{
  "name": "${this.options.name}"
\}
`
      )
    }
    catch(err) {
      logerror(`An error occured while writing to ${CONFIG_FILENAME}: ${err}`)
      return false
    }
    logp(`Created ${file(CONFIG_FILENAME)}`)
    return true
	}

  initOpenAPI():boolean {
    // fs.readFile(OPENAPI_TEMPLATE_FILENAME, (err, data) => {
    //   if(err) {
    //     console.error(`Error reading file ${OPENAPI_TEMPLATE_FILENAME}: ${err}`)
    //     return
    //   }

      // let openApiTemplate = JSON.parse(data.toString());
      this.addInfo(openApiTemplate.info);
      new ApiManager().writeApiJson(openApiTemplate); // WARNING: ApiManager must be created here since the blackbox config will not have been created in advance
    // })

    logp(`Created ${file(OPENAPI_FILENAME)}`)
    return true
	}

	addInfo(info:any) {
		info.version = appConfig.version
		info.title = this.options.title
		if(this.options.description && this.options.description !== '')
			info.description = this.options.description
	}
}
