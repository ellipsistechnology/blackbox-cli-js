import { jsonTypes } from "./ApiManager"

export function toCammelCase(name: string, startUpper: boolean = true) {
    if(jsonTypes.includes(name) || name.endsWith('[]') && jsonTypes.includes(name.substring(0, name.length-2))) {
      return name
    } else {
      const cc = name.replace(/([-_][a-z])/ig, ($1) => {
        return $1.toUpperCase()
          .replace('-', '')
          .replace('_', '')
      })
      if(startUpper) {
        return cc.charAt(0).toUpperCase() + cc.substring(1)
      } else {
        return cc
      }
    }
    // name.split('-')
    //   .map((part, i) => i > 0 || startUpper ? part.charAt(0).toUpperCase() + part.substring(1) : part)
    //   .join('')
  }
