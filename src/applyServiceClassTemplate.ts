import {importMap} from './Generator'
import { toCammelCase } from './utils'

interface TemplateMap {
  name:string
  methods:{
    type:string
    operationId:string
  }[]
}

export default function applyServiceClassTemplate({name, methods}:TemplateMap):string {

  const className = toCammelCase(name)
  const classVarName = toCammelCase(name, false)

  let imports = ''
  new Set(methods // use a set to remove duplicates
    .map(method=>method.type) // extract types from methods
    .filter(type=>type) // remove empty
    .map((type:string) => type.endsWith('[]') ? type.substring(0, type.length-2) : type)
    .reduce((classes, type)=>classes.concat(type.split('&')), <string[]>[]) // split Type1&Type2
    .reduce((classes, type)=>classes.concat(type.split('|')), <string[]>[]) // split Type1|Type2
  ).forEach(type => imports += (<any>importMap)[type] ? `import {${type}} from '${(<any>importMap)[type]}'\n` : `import ${type} from './${type}'\n`)

  return `import {serviceClass, autowired} from 'blackbox-ioc'
import {makeServiceObject} from 'blackbox-services'
${imports}

@serviceClass('${classVarName}-service')
export class ${className}ServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  constructor() {
  }
  ${methods.map( method => `
  ${method.operationId}(props:any)${method.type?`:${method.type}`:''} {
    ${method.type === 'Service' ?
    `return makeServiceObject(this.oasDoc, '${name}')`:
    `throw new Error(\`${method.operationId} not yet implemented. Called with props=\${JSON.stringify(props)}\`)`}
  }`).join('\n')}
}
`
}
