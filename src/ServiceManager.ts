import CliOptions from './CliOptions'
import ApiManager, {jsonTypes} from './ApiManager'
import {logerror, logp, code, logem} from './logging'
import template from 'json-templater/object'
import {printList} from './print-helper'
import readlineSync from 'readline-sync'

import post_template from './templates/post-template.json'
import put_template from './templates/put-template.json'
import patch_template from './templates/patch-template.json'
import delete_template from './templates/delete-template.json'
import service_template from './templates/service-template.json'
import RepositoryManager from './RepositoryManager';

import DatatypeManager from './DatatypeManager';
import { toCammelCase } from './utils'

const methodToOperation:{ [key: string]: string } = {
  get: "get",
  post: "create",
  put: "replace",
  patch: "update",
  delete: "delete",
  options: "getOptionsFor"
}

export default class ServiceManager {
  options:CliOptions;
  apiManager:ApiManager;

  constructor(options:CliOptions) {
    this.options = options
    this.apiManager = new ApiManager();
  }

  async add() {
    // Validate options:
    if(this.options.name === '') {
      logerror("Name not found. Use -n or --name to supply a name for the service you wish to add.")
      return
    }

    // Load and configure OpenAPI JSON:
    try {
      const openApiRoot = await this.apiManager.loadOpenApiJson()
  		this.addPathForService(openApiRoot);
      // this.addReference(openApiRoot, this.options.service === '' ? "/" : "/"+this.options.service);

  		if(this.options.datatype !== '')
  			await this.setServiceDatatype(openApiRoot);

  		this.apiManager.writeApiJson(openApiRoot);

      logem(`Successfully added service ${code(this.options.name)}`)
    }
    catch(err) {
      logerror(`Failed to add service: ${err}`, err)
    }
  }

  // TODO: allow for updating methods
  async update() {
    // Set service datatype:
		if(this.options.datatype !== '') {
      try {
			  const openApiJson = await this.apiManager.loadOpenApiJson()

				await this.setServiceDatatype(openApiJson)
				this.apiManager.writeApiJson(openApiJson)
        logem(`Successfully updated service ${code(this.options.servicePath())} with datatype ${code(this.options.datatype)}`)
      }
      catch(err) {
        logerror(`Failed to update service: ${err}`, err)
      }
		}
  }

  private addPathForService(openApiRoot:any) {
		const path = this.options.servicePath();
		if(openApiRoot.paths[path]) {
			throw new Error("A service already exists at path "+path)
		}
		else {
			let service = template(
				service_template,
				{
          summary: this.options.summary ? this.options.summary : '',
          tag: this.options.serviceTag(),
          getOperationId: this.makeOperationId('get', 'Service'),
          optionsOperationId: this.makeOperationId('options', 'Service'),
          parameters: [
            { "$ref": "#/components/parameters/query-depth" },
            { "$ref": "#/components/parameters/query-verbose" }
          ]
        }
			);
			openApiRoot.paths[path] = service
      logp(`Added path ${code(path)}`)
		}
    return true
	}

  private makeOperationId(method:string, suffix = ''):any { // e.g. deleteDatatype
    const name = this.options.singular ? this.options.singular : this.options.name
    return (
      methodToOperation[method] +
      toCammelCase(name) +
      suffix
    )
  }

  private addServiceMethod(
    servicePath:string,
    httpMethod:string,
    openApiJson:any,
		methodTemplate:any,
    templateMap:any) {

    templateMap = Object.assign(
      { operationId: this.makeOperationId(httpMethod) },
      templateMap
    )

    let serviceElement = openApiJson.paths[servicePath]
		if(!serviceElement[httpMethod]) {
			let method = template(methodTemplate, templateMap)
			if(method) {
        if(method.requestBody)
          method.requestBody.content['application/json'].schema = this.schema()
				serviceElement[httpMethod] = method
        logp(`Added ${code(httpMethod)} to path ${code(servicePath)}`)
      }
      else {
        logerror(`Error loading template for method ${methodTemplate}`)
      }
		}
	}

  private schema():any {
		if((<any>jsonTypes).includes(this.options.datatype))
			return { type: this.options.datatype }
    else
			return { "$ref": `#/components/schemas/${this.options.datatype}` }
	}

  private httpGetResponseSchema():any {
		if(!(<any>jsonTypes).includes(this.options.datatype)) {
			return {
        allOf: [
  				this.schema(),
  				{ "$ref": "#/components/schemas/verbose-object" }
  			]
      }
		}
		else {
			return this.schema()
		}
	}

  private addServiceGet(openApiJson:any) {
    const path = this.options.servicePath()
		let getSchemaParent:any = this.apiManager.getSchemaParentByPath(openApiJson, path)
		getSchemaParent.schema = {
		  type: "array",
		  items: this.httpGetResponseSchema()
		}

    if(this.options.summary)
      openApiJson.paths[path].get.summary = this.options.summary
    else if(!openApiJson.paths[path].get.summary)
      openApiJson.paths[path].get.summary = `Retrieve a list of ${this.options.datatype} objects.`

    openApiJson.paths[path].get.operationId = this.makeOperationId('get', 'List')

    logp(`Set get schema for path ${code(path)}`)
	}

  private addServiceWithName(openApiJson:any, templateMap:any):any {
    const serviceName = this.options.servicePath()+"/{name}"
    let service = openApiJson.paths[serviceName]
    let getOperationId = this.makeOperationId('get')
    let optionsOperationId = this.makeOperationId('options')
    if(!service) {
		  service = template(
        service_template,
        {
          summary: "Gets a "+this.options.name+" by name.",
          tag: this.options.serviceTag(),
          getOperationId,
          optionsOperationId
        }
      )

      service.options.parameters = service.get.parameters = [
        {
          "$ref": "#/components/parameters/path-name"
        },
        {
          "$ref": "#/components/parameters/query-verbose"
        },
        {
          "$ref": "#/components/parameters/query-depth"
        }
      ]

      openApiJson.paths[serviceName] = service
      logp(`Addded service to path ${code(serviceName)}`)
		}
    else {
      service.operationId = getOperationId
    }
		return service
	}

  private setupServiceWithNameGet(service:any) {
    let tags = service.get.tags
    if(!tags) {
	    tags = []
      service.get.tags = tags
    }
    const tag = this.options.service ? this.options.service : this.options.name;
    if(!(<any>tags).includes(tag))
		  tags.push(tag)

		service.get.responses[200].content["application/json"].schema = this.httpGetResponseSchema()
	}

  private async setServiceDatatype(openApiJson:any) {

    // Check the datatype exists or load it from a repository:
    // await this.addDatatypeToSchema(openApiJson, this.options.datatype)
    const repo = await new RepositoryManager(this.options).findRepositoryForDatatype(this.options.datatype)
    if(repo) {
      new DatatypeManager(this.options).addDependantDatatypes(openApiJson, repo, this.options.datatype)

      // Add to x-blackbox-types array:
      const service = openApiJson.paths[this.options.servicePath()]
      if(!service)
        throw new Error(`Service not found at path ${this.options.servicePath()}`)
      if(!service['x-blackbox-types'])
        service['x-blackbox-types'] = []
      service['x-blackbox-types'].push({
        uri: repo.uri,
        name: this.options.datatype
      })
    }

    // Add methods from templates:
		let templateData = {
			datatype: this.options.datatype,
			tag: this.options.serviceTag(),
			service: this.options.name
		}
		let methods = this.options.methodsSet()

		this.addServiceGet(openApiJson); // FIXME: generating incorrect operationId when getting here from add()

		if((<any>methods).includes("post"))
			this.addServiceMethod(this.options.servicePath(), "post", openApiJson, post_template, templateData)

    if((<any>methods).includes("put") || (<any>methods).includes("patch") || (<any>methods).includes("delete")) {

      let serviceWithName = this.addServiceWithName(openApiJson, templateData)
      this.setupServiceWithNameGet(serviceWithName);

  		let serviceWithNamePath = this.options.servicePath()+"/{name}"
  		if((<any>methods).includes("put"))
  			this.addServiceMethod(serviceWithNamePath, "put", openApiJson, put_template, templateData)
  		if((<any>methods).includes("patch"))
  			this.addServiceMethod(serviceWithNamePath, "patch", openApiJson, patch_template, templateData)
  		if((<any>methods).includes("delete"))
  			this.addServiceMethod(serviceWithNamePath, "delete", openApiJson, delete_template, templateData)
    }
	}

  list() {
    this.apiManager.loadOpenApiJson()
    .then( (openApiDoc) => {
      printList(Object.keys(openApiDoc.paths))
    })
    .catch( (err) => {
      logerror(`Error loading openAPI document`)
    })
  }

  delete() {
    // Validate parameters:
    if(this.options.name === '') {
      logerror("Name not found. Use -n or --name to supply a name for the service you wish to delete.")
      return
    }

    // Prompt user for confirmation to delete:
		let message = "Are you sure you want to delete service "+this.options.servicePath()+(this.options.all?" and all it's children":"")+"? (y/n): "
		if(readlineSync.question(message) === 'y') {

      this.apiManager.loadOpenApiJson()
      .then( (openApiJson) => {
        // Find the path to delete:
  			let servicePath = this.options.servicePath()
  			if(openApiJson.paths[servicePath]) {

  				// Delete the service:
  				logp("Deleting path "+code(servicePath));
  				openApiJson.paths[servicePath] = undefined
  				let servicePathWithName = servicePath+"/{name}"
  				if(openApiJson.paths[servicePathWithName]) {
  					logp("Deleting path "+code(servicePathWithName))
  					openApiJson.paths[servicePathWithName] = undefined
  				}

  				// Delete any child services:
  				if(this.options.all) {
  					Object.keys(openApiJson.paths)
  						.filter((key) => key.startsWith(servicePath+"/"))
  						.forEach((key) => {
  							logp("Deleting path "+code(key))
  							openApiJson.paths[key] = undefined
  						})
  				}

  				// Write changes:
  				this.apiManager.writeApiJson(openApiJson)

          logem(`Successfully deleted service ${code(servicePath)}.`)
  			}
  			else {
  				logerror(`Service not found for path ${code(servicePath)}.`)
  			}
      })
      .catch( (err) => {
        logerror(`Error loading openAPI document: ${err}`, err)
      })
		}
  }
}
