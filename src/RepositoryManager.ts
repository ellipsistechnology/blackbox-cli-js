import CliOptions from "./CliOptions";
import ApiManager from './ApiManager'
import { logerror, logp, code, logem } from "./logging";
import {CONFIG_FILENAME} from './ApiManager'
import { printList } from "./print-helper";
import fs from 'fs'
import axios from 'axios'
import filenamifyUrl from 'filenamify-url'
import fileUrl from 'file-url'

export interface RepoWrapper {
  uri: string
  repo: any
}

export default class RepositoryManager {
  options:CliOptions;
  apiManager: ApiManager;

  constructor(options:CliOptions){
    this.options = options
    this.apiManager = new ApiManager();
  }

  private ensureRepositories(bbDoc:any, repo:{file?:string, url?:string}) {
    if(!bbDoc.repositories)
      bbDoc.repositories = []
    bbDoc.repositories.forEach((rep:any) => {
      if(repo.file && rep.file === repo.file || repo.url && rep.url === repo.url)
        throw new Error(`Repository ${JSON.stringify(repo)} already exists in ${CONFIG_FILENAME}.`)
    })
  }

  add() {
    if(this.options.file !== "") {
      this.apiManager.loadConfigJson()
      .then( bbDoc => {
        const repo = {
          file: this.options.file
        }
        this.ensureRepositories(bbDoc, repo)
        bbDoc.repositories.push(repo)
        this.apiManager.writeConfigJson(bbDoc)
        logem(`Successfully added repository ${code(repo.file)}.`)
      })
      .catch( err => logerror(err) )
    }
    else if(this.options.url) {
      this.apiManager.loadConfigJson()
      .then( bbDoc => {
        const repo = {
          url: this.options.url
        }
        this.ensureRepositories(bbDoc, repo)
        bbDoc.repositories.push(repo)
        this.apiManager.writeConfigJson(bbDoc)
        logem(`Successfully added repository ${code(repo.url)}.`)
      })
      .catch( err => logerror(err) )
    }
    else {
      logerror(`Repository must be specified by either a --file or --url option.`)
    }
  }

  list() {
    this.apiManager.loadConfigJson()
    .then( bbDoc => {
      printList(bbDoc.repositories.map((repo:any) => repo.file ? "file="+repo.file : "url="+repo.url))
    })
    .catch( err => logerror(err) )
  }

  delete() {
    if(this.options.file === "" && this.options.url === "")
      logerror(`File or url must be provided for delete.`)
    this.apiManager.loadConfigJson()
    .then( bbDoc => {
      let repo
      for(let i in bbDoc.repositories){
        if(bbDoc.repositories[i].file === this.options.file || bbDoc.repositories[i].url === this.options.url) {
          repo = bbDoc.repositories[i]
          bbDoc.repositories = bbDoc.repositories.slice(0, i).concat(bbDoc.repositories.slice(i+1))
          break;
        }
      }
      this.apiManager.writeConfigJson(bbDoc)
      logem(`Successfully deleted repository ${code(JSON.stringify(repo))}.`)
    })
    .catch( err => logerror(err) )
  }

  /**
   * Loads a repository from the given path.
   */
  async loadRepositoryFromFile(path:string):Promise<any> {
    const data = await fs.promises.readFile(path)
    return JSON.parse(data.toString())
  }

  // TODO allow for authentication
  async loadRepositoryFromUrl(url:string):Promise<any> {
    // Ensure cache dir excists:
    if(!fs.existsSync('./.bb')) {
      await fs.promises.mkdir('./.bb')
    }
    if(!fs.existsSync('./.bb/repositories')) {
      await fs.promises.mkdir('./.bb/repositories')
    }

    // Create name from url:
    let path = './.bb/repositories/'+filenamifyUrl(url)
    if(!path.endsWith('.json'))
      path += '.json'

    // Load from cache:
    let repo
    if(fs.existsSync(path)) {
      repo = await this.loadRepositoryFromFile(path)
    }

    // If not in cache then load from URL:
    if(!repo) {
      logp(`Downloading repository from ${code(url)}...`)
      const response = await axios.get(url)

      if(response.status !== 200)
        throw new Error(`Download failed: status ${response.status} ${response.statusText}.`)

      if(!response.headers['content-type'].includes('application/json')
          && !response.headers['content-type'].includes('text/plain'))
        throw new Error(`Invalid response content type '${response.headers['content-type']}' received from ${url}.`)

      repo = response.data

      logp(`Download complete.`)
      await fs.promises.writeFile(path, JSON.stringify(repo))
    }

    return repo
  }

  /**
   * Searches repositories for the given datatype.
   * @return A DataTypeWrapper containing the repository's URI and the datatype.
   */
  async findRepositoryForDatatype(datatype: string): Promise<RepoWrapper|undefined> {
    const bbDoc = await this.apiManager.loadConfigJson();
    if(!bbDoc.repositories) {
      return undefined
    }

    for(let repo of bbDoc.repositories) {
      let data:any

      if(repo.file) {
        data = await this.loadRepositoryFromFile(repo.file)
      }
      else if(repo.url) {
        data = await this.loadRepositoryFromUrl(repo.url)
      }

      if(!data) {
        logerror(`Invalid repository found in ${CONFIG_FILENAME}: ${JSON.stringify(repo)}`)
      }
      else if(data[datatype]) {
        return {
          uri: repo.url ? repo.url : fileUrl(repo.file),
          repo: data
        }
      }
    }
    return undefined
  }
}
